import './App.css';
import PrimaryGrid from './containers/PrimaryGrid.jsx'
import Header from './containers/Header.jsx'
import RightSidebar from './containers/RightSidebar/RightSidebar.jsx'
import styled from 'styled-components';
import React from 'react';
import { useSelector } from 'react-redux';
import constants from './util/constants.json'

const AppContainer = styled.div`
  box-sizing: border-box;
  padding: 2% 0 2% 2%;
  height: ${(props) => props.isSidebarOpen ? '150vh' : '190vh'};
  width: 100vw;
  transition: height ${constants.transitionTimes.toggleSidebar};
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalSecond}){
    width: 200vw;
  }
  @media (max-height: ${constants.screenDimensionBreakpoints.verticalSecond}){
    height: ${(props) => props.isSidebarOpen ? '300vh' : '380vh'};
  }
`;

function App() {
  const isSidebarOpen = useSelector((state) => state.rightSidebar.isOpen)
  const hasSidebarBeenToggled = useSelector((state) => state.rightSidebar.hasSidebarBeenToggled)
  return (
    <AppContainer hasSidebarBeenToggled={hasSidebarBeenToggled} isSidebarOpen={isSidebarOpen} >
      <Header />
      <PrimaryGrid />
      <RightSidebar />
    </AppContainer>
  );
}

export default App;
