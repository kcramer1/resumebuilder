import * as headerActType from '../actionTypes/header';
import * as primaryGridActType from '../actionTypes/primaryGrid';
import * as rightSidebarActType from '../actionTypes/rightSidebar';
import * as secondaryGridsActType from '../actionTypes/secondary';
import * as tertiaryGridsActType from '../actionTypes/tertiary';
import * as tutorialActType from '../actionTypes/tutorial';
import constants from '../../util/constants.json'

const actType = { ...headerActType, ...primaryGridActType, ...rightSidebarActType, ...secondaryGridsActType, ...tertiaryGridsActType, ...tutorialActType }

export const initialState = {
  tutorialIndex: 0,
  primaryTopLeftToggle: false,
  secondaryTopLeftToggle: false,
  persistSecondaryGridToggle: false,
  persistTertiaryGridToggle: false,
  setColorToggle: false,
  setIsTargetColorFontToggle: false,
  setTextToggle: false,
  setFontFamilyToggle: false,
  setFontSizeToggle: false,
  setIsBoldToggle: false,
  setMarginToggle: false,
  selectViewToggle: false,
  selectAlaToggle: false,
  selectEditToggle: false,
  nextToggle: false,
  exitToggle: false,
  isColorBoxRendered: false,
  setIsPrimaryActiveToggle: false,
  setSecondaryToActiveToggle: false
};

const tutorialReducer = (state = initialState, action) => {
  const {type, payload} = action
  switch (type) {

    case actType.INCREASE_TUTORIAL_INDEX:
      {
        if(payload) {
          return {
            ...state,
            tutorialIndex: state.tutorialIndex + 1
          }
        } else {
          return state
        }
      }
    case actType.SET_COLOR_BOX_RENDERED:
      {
        return {
          ...state,
          isColorBoxRendered: payload
        }
      }
    case actType.EXIT_TUTORIAL:
      {
        return {
          ...state,
          tutorialIndex: -1
        };
      }
    case actType.TOGGLE_NEXT:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            nextToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_SECONDARY_TO_ACTIVE:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setSecondaryToActiveToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_IS_PRIMARY_ACTIVE:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setIsPrimaryActiveToggle: true
          };
        } else {
          return state
        }
      }

    case actType.SET_PRIMARY_TOP_LEFT:
      {
        if(state.tutorialIndex > 0 && payload.length) {
          return {
            ...state,
            primaryTopLeftToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_SECONDARY_TOP_LEFT:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            secondaryTopLeftToggle: true
          };
        } else {
          return state
        }
      }
    case actType.PERSIST_SECONDARY:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            persistSecondaryGridToggle: true
          };
        } else {
          return state
        }
      }
    case actType.PERSIST_TERTIARY:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            persistTertiaryGridToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_COLOR:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setColorToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_IS_COLOR_TARGET_FONT:
      {
        if(state.tutorialIndex > 0 && payload) {
          return {
            ...state,
            setIsTargetColorFontToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_TEXT:
      {
        if (payload.length > 4 && payload !== constants.textInputPlaceholder && state.tutorialIndex > 0) {
          return {
            ...state,
            setTextToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_FONT_FAMILY:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setFontFamilyToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_FONT_SIZE:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setFontSizeToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_IS_BOLD:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setIsBoldToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_MARGIN:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            setMarginToggle: true
          };
        } else {
          return state
        }
      }
    case actType.SET_MODE_INDEX:
      {
        if (payload === '2' && state.tutorialIndex > 0) {
          return {
            ...state,
            selectViewToggle: true
          };
        } else if (payload === '1' && state.tutorialIndex > 0) {
          return {
            ...state,
            selectEditToggle: true
          };
        } else if (payload === '3' && state.tutorialIndex > 0) {
          return {
            ...state,
            selectAlaToggle: true
          };
        } else {
          return state
        }
      }
    case actType.TOGGLE_EXIT:
      {
        if(state.tutorialIndex > 0) {
          return {
            ...state,
            exitToggle: true
          };
        } else {
          return state
        }
      }
    case actType.RESET_TOGGLES:
      {  
        const stateCopy = {...state}
        for (let stateKey in stateCopy) {
          if (typeof stateCopy[stateKey] === 'boolean') {
            stateCopy[stateKey] = false
          }
        }
          return {
            ...stateCopy
          };
      }
    default:
      return state;
  }
};

export default tutorialReducer;
