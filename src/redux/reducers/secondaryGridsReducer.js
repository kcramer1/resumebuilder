import * as actType from '../actionTypes/secondary';
import {secondaryGrids} from '../../util/iceCream'
import dcopy from 'deep-copy';

export const initialState = {
  grids: [],
  currentGridIndex: -1,
  topLeft: []
};


const secondaryGridsReducer = (state = initialState, action) => {
  const newGrids = dcopy(state.grids);
  const {type, payload} = action

  switch (type) {
    case actType.PERSIST_SECONDARY:
      newGrids.push(payload)
      return {
        ...state,
        grids: newGrids,
        currentGridIndex: newGrids.length - 1
      };
    case actType.SET_SECONDARY_TOP_LEFT:
      {
        return {
          ...state,
          topLeft: payload
        };
      }
    case actType.DELETE_SECONDARY:
      newGrids.splice(payload, 1, null)
      return {
        ...state,
        grids: newGrids,
        currentGridIndex: -1
      };
    case actType.SET_SECONDARY_TO_INACTIVE:
      const newSecondaryGrids = newGrids.map((grid) => {
        if (grid) {
          grid.isActive = false;
        }
        return grid;
      })
      return {
        ...state,
        grids: newSecondaryGrids,
        currentGridIndex: -1
      };
    case actType.SET_SECONDARY_TO_ACTIVE:
      newGrids[payload].isActive = true;
      return {
        ...state,
        grids: newGrids,
        currentGridIndex: payload
      };
    case actType.SET_SECONDARY_BACKGROUND_COLOR:
      newGrids[payload.secondaryIndex].backgroundColor = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_FONT_COLOR:
      newGrids[payload.secondaryIndex].fontColor = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_TEXT:
      newGrids[payload.secondaryIndex].text = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_FONT_FAMILY:
      newGrids[payload.secondaryIndex].fontFamily = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_FONT_SIZE:
      newGrids[payload.secondaryIndex].fontSize = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_IS_BOLD:
      newGrids[payload.secondaryIndex].isBold = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_IS_ITALIC:
      newGrids[payload.secondaryIndex].isItalic = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_IS_UNDERLINED:
      newGrids[payload.secondaryIndex].isUnderlined = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.SET_SECONDARY_MARGIN:
      newGrids[payload.secondaryIndex].marginArray = payload.value;
      return {
        ...state,
        grids: newGrids
      };
    case actType.ADD_SECONDARY_COLUMN:
      {
        const newGrid = newGrids[payload];
        const columnArray = newGrid.column.split('/')
        const columnLimit = columnArray[columnArray.length - 1]
        const newColumnLimit = parseInt(columnLimit, 10) + 1
        const newColumn = `${columnArray[0]}/${newColumnLimit}`
        newGrid.column = newColumn
        newGrid.xRepeat += 1
        newGrids.splice(payload, 1, newGrid)
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.ADD_SECONDARY_ROW:
      const newGrid = newGrids[payload];
      const rowArray = newGrid.row.split('/')
      const rowLimit = rowArray[rowArray.length - 1]
      const newRowLimit = parseInt(rowLimit, 10) + 1
      const newRow = `${rowArray[0]}/${newRowLimit}`
      newGrid.row = newRow
      newGrid.yRepeat += 1
      newGrids.splice(payload, 1, newGrid)
      return {
        ...state,
        grids: newGrids
      };
    case actType.DEPLOY_SECONDARY_ICE_CREAM:
      {
      const deepCopy = dcopy(secondaryGrids)
        return {
          currentGridIndex: -1,
          topLeft: [],
          grids: deepCopy
        };
      }
    default:
      return state;
  }
};

export default secondaryGridsReducer;
