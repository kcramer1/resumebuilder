import * as actType from '../actionTypes/header';

export const initialState = {
  modeIndex: '1',
  isModalVisible: false
};

const headerReducer = (state = initialState, action) => {
  const {type, payload} = action
  // const stateCopy = {...state}
  switch (type) {
    case actType.SET_MODE_INDEX:
      {
        return {
          ...state,
          modeIndex: payload
        };
      }
    case actType.SET_MODAL_IS_VISIBLE:
      {
        return {
          ...state,
          isModalVisible: payload
        };
      }
    default:
      return state;
  }
};

export default headerReducer;
