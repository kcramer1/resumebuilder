import { combineReducers } from 'redux';
import secondaryGridsReducer from './secondaryGridsReducer';
import tertiaryGridsReducer from './tertiaryGridsReducer';
import rightSidebarReducer from './rightSidebarReducer';
import headerReducer from './headerReducer';
import primaryGridReducer from './primaryGridReducer';
import tutorialReducer from './tutorialReducer';

const rootReducer = (
  state,
  action
) => {
  return combineReducers({
    secondaryGrids: secondaryGridsReducer,
    tertiaryGrids: tertiaryGridsReducer,
    rightSidebar: rightSidebarReducer,
    header: headerReducer,
    primaryGrid: primaryGridReducer,
    tutorial: tutorialReducer
  })(state, action);
};

export default rootReducer;
