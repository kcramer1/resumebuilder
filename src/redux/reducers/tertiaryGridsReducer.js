import * as actType from '../actionTypes/tertiary';
import {tertiaryGrids} from '../../util/iceCream'
import dcopy from 'deep-copy';

export const initialState = {
  grids: [],
  currentGridIndex: [-1, -1]
};


const tertiaryGridsReducer = (state = initialState, action) => {
  const newGrids = dcopy(state.grids);
  const {type, payload} = action
  switch (type) {
    case actType.PERSIST_TERTIARY: 
      {
        const oldGrid = newGrids[payload.secondaryParentIndex];
        oldGrid.push(payload.gridProps);
        const newGrid = dcopy(oldGrid);
        newGrids[payload.secondaryParentIndex] = newGrid;
        return {
          ...dcopy(state),
          grids: newGrids,
          currentGridIndex: [payload.secondaryParentIndex, newGrid.length - 1]
        };
      }
    case actType.ADD_TERTIARY_PLACEHOLDER:
      {
        newGrids.push([])
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.DELETE_TERTIARY:
      {
        newGrids[state.currentGridIndex[0]][state.currentGridIndex[1]] = null;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_TO_INACTIVE:
      {
        const newGridsArray = [];  
        for (const grids of dcopy(state.grids)) {
          const newGrids = grids.map((grid) => {
            if (grid) {
              grid.isActive = false;
            }
            return grid;
          })
          newGridsArray.push(newGrids)
        }
        return {
          ...state,
          grids: newGridsArray,
          currentGridIndex: [-1, -1],
        };
      }
    case actType.SET_TERTIARY_TO_ACTIVE:
      {
        newGrids[payload[0]][payload[1]].isActive = true;
        return {
          ...state,
          grids: newGrids,
          currentGridIndex: payload
        };
      }
    case actType.SET_TERTIARY_BACKGROUND_COLOR:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].backgroundColor = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_FONT_COLOR:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].fontColor = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_TEXT:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].text = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_FONT_FAMILY:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].fontFamily = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_FONT_SIZE:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].fontSize = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_IS_BOLD:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].isBold = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_IS_ITALIC:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].isItalic = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_IS_UNDERLINED:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].isUnderlined = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.SET_TERTIARY_MARGIN:
      {
        newGrids[payload.tertiaryIndex[0]][payload.tertiaryIndex[1]].marginArray = payload.value;
        return {
          ...state,
          grids: newGrids
        };
      }
    case actType.DEPLOY_TERTIARY_ICE_CREAM:
      {
        const deepCopy = dcopy(tertiaryGrids)
        return {
          currentGridIndex: [-1, -1],
          grids: deepCopy
        };
      }
    default:
      return state;
  }
};

export default tertiaryGridsReducer;
