import * as actType from '../actionTypes/primaryGrid';

export const initialState = {
  isActive: true,
  topLeft: []
};

const primaryGridReducer = (state = initialState, action) => {
  const {type, payload} = action
  switch (type) {
    case actType.SET_IS_PRIMARY_ACTIVE:
      {
        return {
          ...state,
          isActive: payload
        };
      }
    case actType.SET_PRIMARY_TOP_LEFT:
      {
        return {
          ...state,
          topLeft: payload
        };
      }
    default:
      return state;
  }
};

export default primaryGridReducer;
