import * as actType from '../actionTypes/rightSidebar';
import constants from '../../util/constants.json'

export const initialState = {
  isOpen: true,
  isTargetColorFont: false,
  fontFamily: constants.fontFamilyArray[0],
  fontSize: constants.fontSizeArray[0],
  isBold: false,
  isItalic: false,
  isUnderlined: false,
  marginArray: [0, 0, 0, 0],
  backgroundColor: constants.colors.sideBarElementBackground,
  fontColor: constants.colors.sideBarElementForeground,
  enteredText: constants.textInputPlaceholder,
  textFocusTrigger: false,
  utilizedColors: new Set()
};

const rightSidebarReducer = (state = initialState, action) => {
  const {type, payload} = action
  const stateCopy = {...state}
  switch (type) {
    case actType.SET_IS_SIDEBAR_OPEN:
      {
        return {
        ...state,
        isOpen: payload
        };
      }
    case actType.SET_COLOR:
      {
        if (state.isTargetColorFont) {
          return {
            ...state,
            fontColor: payload
          };
        } else {
          return {
            ...state,
            backgroundColor: payload
          };
        }
      }
    case actType.SET_FONT_FAMILY:
      { 
        return {
          ...state,
          fontFamily: payload
        };
      }
    case actType.SET_FONT_SIZE:
      {
        return {
          ...state,
          fontSize: payload
        };
      }
    case actType.SET_IS_COLOR_TARGET_FONT:
      {
        return {
          ...state,
          isTargetColorFont: payload
        };
      }  
    case actType.SET_TEXT:
      {
        return {
          ...state,
          enteredText: payload
        };
      }  
    case actType.SET_IS_BOLD:
      {
        return {
          ...state,
          isBold: payload
        };
      }  
    case actType.SET_IS_ITALIC:
      {
        return {
          ...state,
          isItalic: payload
        };
      }  
    case actType.SET_IS_UNDERLINED:
      {
        return {
          ...state,
          isUnderlined: payload
        };
      }  
    case actType.SET_MARGIN:
      {
        const oldMarginArray = [...stateCopy.marginArray]
        const totalXReduction = oldMarginArray[0] + oldMarginArray[2]
        const totalYReduction = oldMarginArray[1] + oldMarginArray[3]
        if (
          (totalXReduction < 5 && (payload === 0 || payload === 2)) ||
          (totalYReduction < 5 && (payload === 1 || payload === 3)) 
          ) {
          oldMarginArray[payload] = oldMarginArray[payload] + 1
          return {
            ...state,
            marginArray: oldMarginArray
          };
        }
        return state
      }  
    case actType.RESET_MARGIN:
      {
        return {
          ...state,
          marginArray: payload || [0, 0, 0, 0]
        };
      }
    case actType.TRIGGER_TEXT_FOCUS:
      {  
        return {
          ...state,
          textFocusTrigger: !state.textFocusTrigger
        };
      }
    case actType.SET_UTILIZED_COLORS:
      { 
        const utilizedColors = [];
        for (let secondaryGrid of payload.secondaryGrids) {
          if (secondaryGrid){
            utilizedColors.push(secondaryGrid.backgroundColor)
            utilizedColors.push(secondaryGrid.fontColor)
          }
        }
        for (let tertiaryGridArray of payload.tertiaryGrids) {
          for (let tertiaryGrid of tertiaryGridArray) {
            if (tertiaryGrid){
              utilizedColors.push(tertiaryGrid.backgroundColor)
              utilizedColors.push(tertiaryGrid.fontColor)
            }
          }
        }
        const utilizedColorsSet = new Set(utilizedColors)
        utilizedColorsSet.delete(undefined)
        return {
          ...state,
          utilizedColors: utilizedColorsSet
        };
      } 
    default:
      return state;
  }
};

export default rightSidebarReducer;
