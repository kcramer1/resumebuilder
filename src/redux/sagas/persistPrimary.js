import { takeEvery } from 'redux-saga/effects';
import * as actType from '../actionTypes/secondary';

export function* persistPrimary(action) {
  const payload = action.payload;
  try {
    yield console.log(payload)
  } catch (err) {
    console.log(err);
  }
}

export default function* watchPersistPrimary() {
  yield takeEvery(actType.PERSIST_SECONDARY, persistPrimary);
}
