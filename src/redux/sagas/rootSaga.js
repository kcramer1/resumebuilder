import { all } from 'redux-saga/effects';
import watchPersistPrimary from './persistPrimary';
import watchFetchPrimary from './fetchPrimary';

export default function* rootSaga() {
  yield all([
    watchPersistPrimary(),
    watchFetchPrimary(),
  ]);
}