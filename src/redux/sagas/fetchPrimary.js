import { takeEvery } from 'redux-saga/effects';
import * as actType from '../actionTypes/secondary';

export function* fetchPrimary(action) {
  const payload = action.payload;
  try {
    yield console.log(payload)
    // const res = yield apiRequest('resumeApi', `/user/${email}`, 'GET');
    // const user = res.data;
    // if (user) {
    //   yield put({type: actType.FETCH_PRIMARY});
    // }
  } catch (err) {
    console.log(err);
  }
}

export default function* watchFetchPrimary() {
  yield takeEvery(actType.FETCH_SECONDARY, fetchPrimary);
}
