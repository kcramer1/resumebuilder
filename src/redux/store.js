import { createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers/rootReducer';
import rootSaga from './sagas/rootSaga';

const prodName = 'prd';
const sagaMiddleware = createSagaMiddleware();
// composeEnhancers enables trace functionality: 
// https://github.com/zalmoxisus/redux-devtools-extension/blob/master/docs/Features/Trace.md
const composeEnhancers = composeWithDevTools({
  trace: true,
  traceLimit: 25
})
const devTools =
  process.env.REACT_APP_env_name !== prodName
    ? composeEnhancers(applyMiddleware(sagaMiddleware))
    : applyMiddleware(sagaMiddleware);

const store = createStore(rootReducer, devTools);
sagaMiddleware.run(rootSaga);

export default store;
