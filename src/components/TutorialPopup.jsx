import styled from 'styled-components';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import constants from '../util/constants.json'
import * as secondaryActType from '../redux/actionTypes/secondary'
import * as tertiaryActType from '../redux/actionTypes/tertiary'
import * as rightSidebarActType from '../redux/actionTypes/rightSidebar'
import * as primaryGridActType from '../redux/actionTypes/primaryGrid'
import * as headerActType from '../redux/actionTypes/header'
import * as tutorialActType from '../redux/actionTypes/tutorial'

const actType = { ...secondaryActType, ...tertiaryActType, ...rightSidebarActType, ...primaryGridActType, ...headerActType, ...tutorialActType }

const ModalContainer = styled.div`
  width: 24rem;
  height: 24rem;
  background-color: #ffffff;
  border: 2px solid #000000;
  border-radius: ${constants.sizes.borderRadius};
  position: fixed;
  left: 50vw;
  top: 50vh; 
  z-index: 300;
  margin-left: -12rem;
  margin-top: -12rem;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: 1fr min-content;
  justify-items: center;
  align-items: center;
  box-shadow: 0 0 8px 2px #000000;
  text-align: center;
  overflow: hidden;
`;

const ModalText = styled.h2`
  margin: 2rem;
  padding: 0;
  border: none;
  grid-column: 1/3;
  grid-row: 1;
`;

const ModalButtons = styled.button`
  margin: 0;
  padding: 0.2rem 0.4rem;
  margin: 0.8rem;
  font-size: 2rem;
  border: none;
  text-align: center;
  text-justify: center;
  grid-row: 2;
  grid-row-gap: 20%;
  grid-column-gap: 20%;
  grid-column: ${props=> props.column};
  border-radius: ${constants.sizes.borderRadius};
  justify-self: ${props => props.isRight? 'end' : 'start'};
  cursor: pointer;
  :hover {
    transform: ${props => props.isMouseDown ? 'scale(1)' : 'scale(1.1)'};
  }
  transition: transform ${constants.transitionTimes.buttonClick};
`;

function TutorialPopup(props) {
  const { shouldDisplayNext, instruction } = props;
  const dispatch = useDispatch()
  const [isMouseDown, setIsMouseDown] = useState(false);

  const handleClick = (e) => {
    if(e.target.id.includes("next")) {
      dispatch({ type: actType.INCREASE_TUTORIAL_INDEX, payload: true })
    }
    else if (e.target.id.includes('exit')) {
      dispatch({ type: actType.EXIT_TUTORIAL})
    }
  }


  return (
    <ModalContainer
      id="modalContainer"
      key="modalContainer"
      onMouseDown={() => setIsMouseDown(true)}
      onMouseUp={() => setIsMouseDown(false)}
      onClick={(e) => handleClick(e)}
    >
      <ModalText>{instruction}</ModalText>
      <ModalButtons id="exitButton" key="exitButton" column={2} isRight isMouseDown={isMouseDown}>Exit</ModalButtons>
      { shouldDisplayNext && <ModalButtons id="nextButton" key="nextButton" column={1} isMouseDown={isMouseDown}>Next</ModalButtons> }
    </ModalContainer>
  );
}

export default TutorialPopup;
