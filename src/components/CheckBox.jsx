import { useDispatch, useSelector } from 'react-redux';
import { useState } from 'react';
import styled from 'styled-components';
import constants from '../util/constants.json'
import * as actTypeRightSidebar from '../redux/actionTypes/rightSidebar';
import * as actTypeSecondary from '../redux/actionTypes/secondary';

const actType = {...actTypeRightSidebar, ...actTypeSecondary}

const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  // Hide checkbox visually but remain accessible to screen readers.
  // Source: https://polished.js.org/docs/#hidevisually
  border: 0;
  clip: rect(0 0 0 0);
  /* clippath: inset(50%); */
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`

const Icon = styled.svg`
  fill: none;
  stroke: ${(props) => props.checkmarkColor};
  stroke-width: 2px;
`

const StyledCheckbox = styled.div`
  display: inline-block;
  box-sizing: border-box;
  width: ${constants.sizes.sidebarInputElement};
  height: ${constants.sizes.sidebarInputElement};
  background-color: ${ props => props.checked ? props.checkedColor : props.uncheckedColor };
  border-radius: ${constants.sizes.borderRadius};
  transition: all ${constants.transitionTimes.checkBoxAnimation};
  margin-right: ${parseInt(constants.sizes.sidebarInputElement, 10) / 3}rem;
  cursor: pointer;
  ${(props) => props.isFocused && `border: 3px solid ${constants.colors.sideBarElementForeground};`}
  ${Icon} {
    visibility: ${props => props.checked ? 'visible' : 'hidden'};
  };
  :hover{
    border: 3px solid ${constants.colors.sideBarElementForeground};
  };
`

const CheckboxAndLabelContainer = styled.div`
  white-space: nowrap;
  @media (max-height: ${constants.screenDimensionBreakpoints.verticalThird}){
    opacity: 0;
    display: none;
  }
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalFirst}){
    padding-top: 1.5%;
    padding-bottom: 1.5%;
  }
  :focus{
    border: 3px solid ${constants.colors.sideBarElementForeground};
  };
`

const CheckboxContainer = styled.div`
  display: flex;
  align-items: center;
  :focus{
    border: 3px solid ${constants.colors.sideBarElementForeground};
  };
`

const CheckboxSpan = styled.span`
  color: ${props => props.fontColor};
  font-size: ${constants.sizes.sidebarInputText};
  position: relative;
  :focus{
    border: 3px solid ${constants.colors.sideBarElementForeground};
  };
`;

const labelTextArray = constants.checkBoxArray

function CheckBox(props) {
  const {uncheckedColor, checkedColor, checkmarkColor, labelTextIndex, column, row, fontColor } = props;
  const isChecked = useSelector((state) => state.rightSidebar[`is${labelTextArray[labelTextIndex]}`])
  const [isFocused, setIsFocused] = useState(false);
  const dispatch = useDispatch();

  const handleCheckboxChange = e => {
    if (labelTextArray[labelTextIndex] === "Bold") {
      dispatch({ type: actType.SET_IS_BOLD, payload: e.target.checked })
    }
    else if (labelTextArray[labelTextIndex] === "Italic") {
      dispatch({ type: actType.SET_IS_ITALIC, payload: e.target.checked })
    }
    else if (labelTextArray[labelTextIndex] === "Underlined") {
      dispatch({ type: actType.SET_IS_UNDERLINED, payload: e.target.checked })
    }
  }

  const handleOnFocus = (focusBoolean) => {
    setIsFocused(focusBoolean)
  }

  const handleEnter = (e) => {
    e.target.checked = !e.target.checked;
    handleCheckboxChange(e)
  }

  return (
    <CheckboxAndLabelContainer
      id={`checkboxAndLabelContainer-${column}-${row}`}
      key={`checkboxAndLabelContainer-${column}-${row}`}
      column={column} 
      row={row}
    >
      <label id={`checkboxLabel-${column}-${row}`}  key={`checkboxLabel-${column}-${row}`} >
        <CheckboxContainer
          id={`checkboxContainer-${column}-${row}`}
          key={`checkboxContainer-${column}-${row}`}
          onChange={(e) => handleCheckboxChange(e)}
          onKeyPress={
            (e) => {
              if (e.key === 'Enter') {
                handleEnter(e)
              }
            }
          }    
        >
          <HiddenCheckbox 
            id={`hiddenCheckbox-${column}-${row}`} 
            key={`hiddenCheckbox-${column}-${row}`}
            onFocus={() => handleOnFocus(true)}
            onBlur={() => handleOnFocus(false)}
          />
          <StyledCheckbox
            id={`styledCheckbox-${column}-${row}`}
            key={`styledCheckbox-${column}-${row}`}
            checked={isChecked} 
            checkedColor={checkedColor}
            uncheckedColor={uncheckedColor}
            isFocused={isFocused}
          >
            <Icon 
              id={`checkboxIcon-${column}-${row}`} 
              key={`checkboxIcon-${column}-${row}`} 
              checkmarkColor={checkmarkColor} 
              viewBox="0 0 24 24"
            >
              <polyline 
                id={`polyline-${column}-${row}`} 
                key={`polyline-${column}-${row}`}
                points="20 6 9 17 4 12" 
              />
            </Icon>
          </StyledCheckbox>
          <CheckboxSpan 
            id={`checkboxLabelSpan-${column}-${row}`}
            key={`checkboxLabelSpan-${column}-${row}`}
            fontColor={fontColor} 
          >
            {labelTextArray[labelTextIndex]}
          </CheckboxSpan>
        </CheckboxContainer>
      </label>
    </CheckboxAndLabelContainer>    
  ) 
}

export default CheckBox;
