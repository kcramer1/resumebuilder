import styled from 'styled-components';
import constants from '../util/constants.json';

const Label = styled.h3`
  position: absolute;
  background-color: ${constants.colors.sideBarElementBackground};
  color: ${constants.colors.sideBarElementForeground};
  z-index: 10;
  margin-right: 0.5rem;
  padding: 0 0.1rem 0 0.1rem;
  border: 0.3rem solid ${constants.colors.sideBarBackground};
  border-radius: 0.75rem;
  font-size: ${constants.sizes.sidebarInputText};
  font-weight: 400;
  bottom: ${`-${(parseInt(constants.sizes.sidebarInputText, 10) * 2) + 1.5}rem`};
  right: 0;
  :hover {
    border-color: ${constants.colors.sideBarElementForeground}
  }
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalFirst}){
    opacity: 0;
    display: none;
  }
`;

function ButtonBoxLabel(props) {
  const {children} = props;

  return (
    <Label>
      {children}
    </Label>
  ) 
}

export default ButtonBoxLabel;
