import { useState } from 'react';
import styled from 'styled-components';
import Select from 'react-select';
import constants from '../util/constants.json'
import { useDispatch, useSelector } from 'react-redux';
import * as actTypeRightSidebar from '../redux/actionTypes/rightSidebar';
import * as actTypeSecondary from '../redux/actionTypes/secondary';

const actType = {...actTypeRightSidebar, ...actTypeSecondary}

const DropDownContainer = styled.div`
  /* grid-column: ${props => props.column};
  grid-row: ${props => props.row}; */
  width: 100%;
  /* box-sizing: border-box; */
  /* border: 5px solid green; */
`

function Dropdown (props) {
  const {selectionArray, column, row, isFontFamily} = props;
  const fontFamily = useSelector((state) => state.rightSidebar.fontFamily);
  const fontSize = useSelector((state) => state.rightSidebar.fontSize);
  const selection = isFontFamily ? fontFamily : fontSize;
  const [isComponentHovered, setIsComponentHovered] = useState(false);
  const dispatch = useDispatch();

  const customStyles = {
    singleValue: (base, state) => ({
      ...base,
      color: constants.colors.sideBarElementForeground,
      fontSize: '1.7rem',
      fontFamily: isFontFamily? state.children : constants.fontFamilyArray[0].value,
      // height: 15,
      // minHeight: 15
    }),
    control: (base, state) => ({
      ...base,
      background: constants.colors.sideBarElementBackground,
      // borderRadius: constants.sizes.borderRadius,
      // height: constants.sizes.sidebarInputElement,
      borderStyle: isComponentHovered ? "solid" : "none !important",
      borderWidth: "2px",
      borderColor: `${constants.colors.sideBarElementForeground} !important`,
      height: "3rem",
      minHeight: "3rem",
      paddingTop: 0,
      top: 0
    }),
    indicatorSeparator: (base, state) => ({
      ...base,
      margin: 0,
      padding: 0,
      backgroundColor: constants.colors.sideBarElementForeground,
      height: "2rem",
      minHeight: "2rem",
      position: "absolute",
      bottom: "0.6rem"
      // marginBottom: "0.5rem",
      // marginTop: "0.5rem"
    }),
    indicatorsContainer: (base, state) => ({
      ...base,
      margin: 0,
      padding: 0,
      position: "relative",
      height: "3rem",
      minHeight: "3rem",
      width: "3rem",
      justifyItems: "center"
    }),
    dropdownIndicator: (base, state) => ({
      ...base,
      margin: 0,
      padding: 0,
      position: "absolute",
      color: `${constants.colors.sideBarElementForeground} !important`,
      bottom: '10px',
      left: '0.8rem',
      height: "1.5rem",
      width: "1.5rem",
      minHeight: "1.5rem",
      minWidth: "1.5rem", 
      maxHeight: "1.5rem",
      maxWidth: "1.5rem", 
      boxSizing: "border-box",
      // height: "3rem",
      // width: "3rem",
      // minHeight: "3rem",
    }),
    menu: (base) => ({
      ...base,
      borderRadius: constants.sizes.borderRadius,
      marginTop: 0
    }),
    menuList: (base) => ({
      ...base,
      background: constants.colors.sideBarElementBackground,
      color: constants.colors.sideBarElementForeground,
      padding: 0
    }),
    option: (base, state) => ({
      ...base,
      fontFamily: state.children,
      fontSize: constants.sizes.sidebarInputText
    })
  };
  
  const handleChange = selectedOption => {
    if (isFontFamily) {
      dispatch({ type: actType.SET_FONT_FAMILY, payload: selectedOption })
    } else {
      dispatch({ type: actType.SET_FONT_SIZE, payload: selectedOption })
    }
    setIsComponentHovered(false)
  };

  return (
    <DropDownContainer 
      id={`dropDownContainer-${column}-${row}`}
      key={`dropDownContainer-${column}-${row}`}
      column={column}
      row={row}
      onMouseEnter={() => setIsComponentHovered(true)}
      onMouseLeave={() => setIsComponentHovered(false)}
    >
      <Select
        styles={customStyles}
        id={`select-${column}-${row}`}
        key={`select-${column}-${row}`}
        value={selection}
        onChange={ (e) => handleChange(e) }
        options={selectionArray}
      />
    </DropDownContainer>
  );
}

export default Dropdown;