import { useState } from 'react';
import styled from 'styled-components';
import indentArrow from './icons/indentArrow.svg';
import reset from './icons/reset.svg';
import resetBackground from './icons/resetBackground.svg';
import indentArrowBackground from './icons/indentArrowBackground.svg';
import { useDispatch } from 'react-redux';
import constants from '../util/constants.json'
import * as actTypeRightSidebar from '../redux/actionTypes/rightSidebar';
import * as actTypeSecondary from '../redux/actionTypes/secondary';

const actType = {...actTypeRightSidebar, ...actTypeSecondary}

const IndentButtonElement = styled.button`
  height: 4rem;
  width: 4rem;
  display: inline-block;
  position: relative;
  vertical-align: middle;
  background-color: inherit;
  border: none;
  padding: 0;
  margin: 0;
  transform: translate(${props => props.isMouseDown ? '0, 5%' : 0});
  transition: all ${constants.transitionTimes.buttonClick};
  cursor: pointer;
  justify-self: center;
  align-self: center;
  grid-column: ${props => props.column};
  grid-row: ${props => props.row};
`

const Icon = styled.img`
  transform: rotate(${(props) => props.directionIndex * 90}deg);
  position: absolute;
  width: 100%;
  top: 0;
  left: 0;
  z-index: 20;
  `;

const IconBackground = styled.img`
  transform: rotate(${(props) => props.directionIndex * 90}deg);
  position: absolute;
  margin-top: ${props => props.isMouseDown ? 0 : '8%'};
  width: 100%;
  top: 0;
  left: 0;
  z-index: 10;
  transition: margin-top ${constants.transitionTimes.buttonClick};
`;

function IndentButton(props) {
  const {directionIndex, column, row, isReset} = props;
  const [isMouseDown, setIsMouseDown] = useState(false)
  const dispatch = useDispatch()
  
  const handleMouseDown = e => {
    setIsMouseDown(true)
  }
  
  const handleMouseUp = e => {
    setIsMouseDown(false)
  }

  const handleClick = e => {
    if (isReset) {
      dispatch({ type: actType.RESET_MARGIN })
    } else {
      dispatch({ type: actType.SET_MARGIN, payload: directionIndex })
    }
  }

  return (
    <IndentButtonElement
      id={`indentButton-${directionIndex}`}
      key={`indentButton-${directionIndex}`}
      column={column} 
      row={row} 
      isMouseDown={isMouseDown} 
      onMouseDown={(e) => handleMouseDown(e)} 
      onMouseUp={e => handleMouseUp(e)}
      onClick={e => handleClick(e)}
    >
      <Icon
        id={`indentIcon-${isReset && 'reset-'}${directionIndex}`}
        key={`indentIcon-${isReset && 'reset-'}${directionIndex}`}
        isMouseDown={isMouseDown} 
        directionIndex={directionIndex} 
        src={isReset ? reset : indentArrow} 
      />
      <IconBackground
        id={`indentIconBackground-${isReset && 'reset-'}${directionIndex}`}
        key={`indentIconBackground-${isReset && 'reset-'}${directionIndex}`}
        isMouseDown={isMouseDown} 
        directionIndex={directionIndex} 
        src={isReset ? resetBackground : indentArrowBackground}
      />
    </IndentButtonElement>
  ) 
}

export default IndentButton;
