import styled from 'styled-components';
import constants from '../util/constants.json'

const StyledH1 = styled.h1`
  padding: 0;
  margin: 0;
  ${props => !!props.column && `grid-column: ${props.column};`}
  ${props => !!props.row && `grid-row: ${props.row};`}
  color: ${(props) => props.fontColor};
  font-family: ${(props) => props.fontFamily.label};
  font-size: ${(props) => props.isSidebarOpen ? `${parseInt(props.fontSize.remSize, 10) * 0.78}rem` : props.fontSize.remSize};
  font-weight: ${(props) => props.isBold? '700' : '400'};
  font-style: ${(props) => props.isItalic? 'italic' : 'normal'};
  text-decoration: ${(props) => props.isUnderlined? 'underline' : 'none'};
  transition: font-size ${constants.transitionTimes.toggleSidebar};
  overflow: scroll;
  word-break: break-all;
`

const StyledH2 = styled.h2`
  padding: 0;
  margin: 0;
  ${props => !!props.column && `grid-column: ${props.column};`}
  ${props => !!props.row && `grid-row: ${props.row};`}
  color: ${(props) => props.fontColor};
  font-family: ${(props) => props.fontFamily.label};
  font-size: ${(props) => props.isSidebarOpen ? `${parseInt(props.fontSize.remSize, 10) * 0.78}rem` : props.fontSize.remSize};
  font-weight: ${(props) => props.isBold? '700' : '400'};
  font-style: ${(props) => props.isItalic? 'italic' : 'normal'};
  text-decoration: ${(props) => props.isUnderlined? 'underline' : 'none'};
  transition: font-size ${constants.transitionTimes.toggleSidebar};
  overflow: scroll;
  word-break: break-all;
`

const StyledH3 = styled.h3`
  padding: 0;
  margin: 0;
  ${props => !!props.column && `grid-column: ${props.column};`}
  ${props => !!props.row && `grid-row: ${props.row};`}
  color: ${(props) => props.fontColor};
  font-family: ${(props) => props.fontFamily.label};
  font-size: ${(props) => props.isSidebarOpen ? `${parseInt(props.fontSize.remSize, 10) * 0.78}rem` : props.fontSize.remSize};
  font-weight: ${(props) => props.isBold? '700' : '400'};
  font-style: ${(props) => props.isItalic? 'italic' : 'normal'};
  text-decoration: ${(props) => props.isUnderlined? 'underline' : 'none'};
  transition: font-size ${constants.transitionTimes.toggleSidebar};
  overflow: scroll;
  word-break: break-all;
`

const StyledH4 = styled.h4`
  padding: 0;
  margin: 0;
  ${props => !!props.column && `grid-column: ${props.column};`}
  ${props => !!props.row && `grid-row: ${props.row};`}
  color: ${(props) => props.fontColor};
  font-family: ${(props) => props.fontFamily.label};
  font-size: ${(props) => props.isSidebarOpen ? `${parseInt(props.fontSize.remSize, 10) * 0.78}rem` : props.fontSize.remSize};
  font-weight: ${(props) => props.isBold? '700' : '400'};
  font-style: ${(props) => props.isItalic? 'italic' : 'normal'};
  text-decoration: ${(props) => props.isUnderlined? 'underline' : 'none'};
  transition: font-size ${constants.transitionTimes.toggleSidebar};
  overflow: scroll;
  word-break: break-all;
`

function TextBox(props) {
  const { fontSize, fontValue, idAndKey} = props;
  let textBoxElement;
  switch (fontSize.value) {
    case 'XLarge':
      textBoxElement = <StyledH1 id={idAndKey} key={idAndKey} {...props} >{fontValue}</StyledH1>;
      break;
    case 'Large':
      textBoxElement = <StyledH2 id={idAndKey} key={idAndKey} {...props} >{fontValue}</StyledH2>;
      break;
    case 'Medium':
      textBoxElement = <StyledH3 id={idAndKey} key={idAndKey} {...props} >{fontValue}</StyledH3>;
      break;
    case 'Small':
      textBoxElement = <StyledH4 id={idAndKey} key={idAndKey} {...props} >{fontValue}</StyledH4>;
      break;
    default:
      textBoxElement = <StyledH4 id={idAndKey} key={idAndKey} {...props} >{fontValue}</StyledH4>;
  }
  return (
    <> {textBoxElement} </>
  ) 
}

export default TextBox;
