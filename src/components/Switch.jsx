import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import leftCarrot from './icons/leftCarrot.svg';
import constants from '../util/constants.json';
import * as actType from '../redux/actionTypes/rightSidebar';


const SwitchContainer = styled.div`
  vertical-align: middle;
  grid-column: ${(props) => props.column};
  grid-row: ${(props) => props.row};
  display: grid;
  grid-column-gap: 1rem;
  grid-template-columns: min-content 6rem min-content;
  justify-items: center;
  align-items: center;
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalThird}){
    grid-column-gap: 0;
  }
`

const HiddenSwitch = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  clip: rect(0 0 0 0);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  white-space: nowrap;
  width: 1px;
  grid-column: 2;
  grid-row: 1;
`

const Icon = styled.img`
  height: 100%;
  position: relative;
  z-index: 20;
  transition: right ${constants.transitionTimes.switchAnimation}, transform ${constants.transitionTimes.switchAnimation};
`

const StyledSwitch = styled.div`
  grid-column: 2;
  grid-row: 1;
  cursor: pointer;
  display: inline-block;
  white-space: nowrap;
  width: ${(props) => `${props.remWidth}rem`};
  height: ${constants.sizes.sidebarInputElement};
  background-color: ${ props => props.checked ? props.checkedColor : props.uncheckedColor };
  ${props => (props.isHovered || props.isFocused) && 'transform: scale(0.95, 0.9);'}
  ${Icon} {
    right: ${
      props => props.checked ? 
        `-${parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;` : 
        `${props.remWidth / 2 +  parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;` 
    };
    transform: ${props => props.checked ? 'scale(1, 1)' : 'scale(-1, 1)'};
  }
  z-index: 50;
  &:before {
    content: "";
    position: relative;
    display: inline-block;
    right: ${parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;
    height: ${constants.sizes.sidebarInputElement};
    width: ${constants.sizes.sidebarInputElement};
    background-color: inherit;
    border-radius: 50%;
    z-index: 0;
  }
  &:after {
    content: "";
    position: relative;
    display: inline-block;
    right: ${parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;
    height: ${constants.sizes.sidebarInputElement};
    width: ${constants.sizes.sidebarInputElement};
    background-color: inherit;
    border-radius: 50%;
  }
`

const StyledSwitchBackground = styled.div`
  grid-column: 2;
  grid-row: 1;
  cursor: pointer;
  display: inline-block;
  white-space: nowrap;
  width: ${(props) => `${props.remWidth}rem`};
  height: ${constants.sizes.sidebarInputElement};
  background-color: ${ constants.colors.sideBarElementForeground };
  ${Icon} {
    right: ${
      props => props.checked ? 
        `-${parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;` : 
        `${props.remWidth / 2 +  parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;` 
    };
    transform: ${props => props.checked ? 'scale(1, 1)' : 'scale(-1, 1)'};
  }
  z-index: 10;
  &:before {
    content: "";
    position: relative;
    display: inline-block;
    right: ${parseInt(constants.sizes.sidebarInputElement, 10) / 2}rem;
    height: ${constants.sizes.sidebarInputElement};
    width: ${constants.sizes.sidebarInputElement};
    background-color: inherit;
    border-radius: 50%;
  }
  &:after {
    content: "";
    position: relative;
    display: inline-block;
    right: -${parseInt(constants.sizes.sidebarInputElement, 10)/2}rem;
    height: ${constants.sizes.sidebarInputElement};
    width: ${constants.sizes.sidebarInputElement};
    background-color: inherit;
    border-radius: 50%;
  }
`

const SwitchSpan = styled.span`
  color: ${(props) => props.color};
  grid-column: ${(props) => props.column};
  font-size: ${constants.sizes.sidebarInputText};
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalThird}){
    font-size: 0;
  }
`;

function CheckBox(props) {
  const {uncheckedColor, checkedColor, leftLabelText, rightLabelText, leftFontColor, rightFontColor, column, row, remWidth} = props;
  const [isHovered, setIsHovered] = useState(false)
  const [isFocused, setIsFocused] = useState(false)
  const isChecked = useSelector((state) => state.rightSidebar.isTargetColorFont)
  const dispatch = useDispatch();

  const handleSwitchChange = e => {
    dispatch({type: actType.SET_IS_COLOR_TARGET_FONT, payload: !isChecked})
  }

  const handleOnFocus = (focusBoolean) => {
    setIsFocused(focusBoolean)
  }

  const handleEnter = (e) => {
    e.target.checked = !e.target.checked;
    handleSwitchChange(e)
  }

  return (
        <SwitchContainer
          id={`switchContainer-${column}=${row}`}
          key={`switchContainer-${column}=${row}`}
          onClick={(e) => handleSwitchChange(e)}
          column={column}
          row={row}
        >
          <HiddenSwitch 
            id={`hiddenSwitch-${column}=${row}`} 
            key={`hiddenSwitch-${column}=${row}`} 
            onFocus={() => handleOnFocus(true)}
            onBlur={() => handleOnFocus(false)}
            onKeyPress={
              (e) => {
                if (e.key === 'Enter') {
                  handleEnter(e)
                }
              }
            }
          />
          <SwitchSpan 
            color={leftFontColor} 
            id={`switchLabelSpan-left-${column}=${row}`}
            key={`switchLabelSpan-left-${column}=${row}`}
            column={1}
            isLeft
          >
            {leftLabelText}
          </SwitchSpan>
          <StyledSwitch
            id={`styledSwitch-${column}=${row}`}
            key={`styledSwitch-${column}=${row}`}
            checked={isChecked} 
            checkedColor={checkedColor}
            uncheckedColor={uncheckedColor}
            remWidth={remWidth}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            isHovered={isHovered} 
            isFocused={isFocused}
          >
            <Icon id={`switchIcon-${column}=${row}`} key={`switchIcon-${column}=${row}`} src={leftCarrot} />
          </StyledSwitch>
          <StyledSwitchBackground />
          <SwitchSpan 
            color={rightFontColor} 
            id={`switchLabelSpan-right-${column}=${row}`}
            key={`switchLabelSpan-right-${column}=${row}`}
            column={3}
            isLeft={false}
          >
            {rightLabelText}
          </SwitchSpan>
        </SwitchContainer>
  ) 
}

export default CheckBox;
