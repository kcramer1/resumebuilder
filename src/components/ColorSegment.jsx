import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';
import constants from '../util/constants.json';
import checkIsDark from '../util/checkIsDark';
import * as actType from '../redux/actionTypes/rightSidebar';

const ColorSegmentButton = styled.button`
  width: 100%;
  height: 100%;
  background-color: ${props => props.color};
  cursor: pointer;
  border-radius: ${constants.sizes.borderRadius};
  border: none;
  /* border: 1px solid #FFFFFF; */
  /* border: ${props => !props.isDark ? '1px solid #000000' : '1px solid #FFFFFF'}; */
  /* box-shadow: ${(props) => props.isMouseDown ? 'none' : `0 1px 0 0 #ffffff`}; */
  /* transform: ${(props) => props.isMouseDown ? 'translate(0, 10px)' : 'none'}; */
  :hover{
    transform: ${(props) => props.isMouseDown ? 'scale(1.1) translate(0, 0.1rem)' : 'scale(1.1)'}
  }
  transition: all ${constants.transitionTimes.buttonClick};
`

function ColorSegment(props) {
  const {color} = props;
  const [isMouseDown, setIsMouseDown] = useState(false);
  const [isDark, setIsDark] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    setIsDark(checkIsDark(color))
  }, [color])

  return (
    <ColorSegmentButton
      id={`colorSegmentButton-${color}`}
      key={`colorSegmentButton-${color}`}
      color={color}
      isDark={isDark}
      isMouseDown={isMouseDown}
      onMouseDown={(e) => {
        setIsMouseDown(true);
      }}
      onMouseUp={() => setIsMouseDown(false)}
      onClick={(e) => {
        const idArray = e.target.id.split('-');
        const color = idArray[idArray.length - 1]
        dispatch({ type: actType.SET_COLOR, payload: color});
      }}
    />
  ) 
}

export default ColorSegment;
