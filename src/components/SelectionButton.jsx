import { useState } from 'react';
import plus from './icons/plus.svg';
import plusWhite from './icons/plusWhite.svg';
import check from './icons/check.svg';
import checkWhite from './icons/checkWhite.svg';
import deleteIcon from './icons/deleteIcon.svg'
import down from './icons/down.svg'
import right from './icons/right.svg'
import styled from 'styled-components';
import constants from '../util/constants.json'

const SelectionButtonElement = styled.div.attrs(props => ({
  style: {
    gridColumn: props.column,
    gridRow: props.row,
    right: props.isHover && props.isActive ? `${parseInt(constants.sizes.selectionCircles, 10) / 2 + (parseInt(constants.sizes.selectionCircles, 10) * .1)}rem` : `${parseInt(constants.sizes.selectionCircles, 10) / 2 }rem`,
    bottom: props.isHover && props.isActive ? `${parseInt(constants.sizes.selectionCircles, 10) / 2 + (parseInt(constants.sizes.selectionCircles, 10) * .1)}rem` : `${parseInt(constants.sizes.selectionCircles, 10) / 2 }rem`,
    height: props.isHover && props.isActive ? `${parseInt(constants.sizes.selectionCircles, 10) * 1.2}rem` : constants.sizes.selectionCircles,
    width: props.isHover && props.isActive ? `${parseInt(constants.sizes.selectionCircles, 10) * 1.2}rem` : constants.sizes.selectionCircles,
    backgroundColor: props.backgroundColor,
    cursor: props.isActive ? 'pointer' : 'cursor',
    opacity: props.isActive ? 1 : 0.3,
    transform: props.isMouseDown  && props.isActive ? 'scale(0.90)' : 'none',
    transition: `all ${constants.transitionTimes.buttonClick}`
  },
}))`
width: 100%;
position: relative;
padding: 0;
border: none;
border-radius: 50%;
z-index: 50;
`

const Icon = styled.img`
  height: 100%;
  position: absolute;
`;

const selectionIconArray = [
  {type: 'plus', icon: plus, darkIcon: plusWhite}, 
  {type: 'check', icon: check, darkIcon: checkWhite}, 
  {type: 'deleteIcon', icon: deleteIcon, darkIcon: deleteIcon}, 
  {type: 'down', icon: down, darkIcon: down}, 
  {type: 'right', icon: right, darkIcon: right}, 
]


function SelectionButton(props) {
  const { column, row, layer, iconIndex, isActive, buttonBackgroundColor, isDark} = props;
  const [isHover, setIsHover] = useState(false);
  const [isMouseDown, setIsMouseDown] = useState(false);
  
  return (
    <SelectionButtonElement 
      id={`${layer}-${selectionIconArray[iconIndex].type}-${isActive ? 'Button' : 'Inactive'}-${column}-${row}`} 
      key={`${layer}-${selectionIconArray[iconIndex].type}-${isActive ? 'Button' : 'Inactive'}-${column}-${row}`}
      backgroundColor={buttonBackgroundColor}
      column={column} 
      row={row}
      isActive={isActive}
      isHover={isHover}
      onMouseEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      onMouseDown={() => setIsMouseDown(true)}
      onMouseUp={() => setIsMouseDown(false)}
      isMouseDown={isMouseDown}
    >
    {
      <Icon 
        src={isDark ? selectionIconArray[iconIndex].darkIcon : selectionIconArray[iconIndex].icon}
        id={`${layer}-${selectionIconArray[iconIndex].type}-${isActive ? 'Button' : 'Inactive'}-Icon-${column}-${row}`} 
        key={`${layer}-${selectionIconArray[iconIndex].type}-${isActive ? 'Button' : 'Inactive'}-Icon-${column}-${row}`} 
      />
    }
  </SelectionButtonElement>
) 
}

export default SelectionButton;
