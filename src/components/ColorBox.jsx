import { useState, useEffect } from 'react';
import ColorSegment from './ColorSegment';
import styled from 'styled-components';
import constants from '../util/constants.json';

const RelativeContainer = styled.div`
  position: relative;
  grid-column:${props => props.column};
  grid-row:${props => props.row};
  padding-bottom: 2rem;
  border: 3px solid ${constants.colors.sideBarElementBackground};
  border-radius: ${constants.sizes.borderRadius};
  
  :hover {
    transform: scale(0.98);
    border-color: ${constants.colors.sideBarElementForeground}
  }
  @media (max-width: 700px){
    opacity: 0;
    display: none;  
  }
  @media (max-height: ${constants.screenDimensionBreakpoints.verticalThird}){
    opacity: 0;
    display: none;
  }

`;

const StyledColorsContainer = styled.div`
  display: grid;
  position: relative;
  box-sizing: border-box;
  height: 100%;
  overflow: scroll;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: 1rem;
  grid-column-gap: 1rem;
  grid-row-gap: 0.7rem;
  padding: 1rem;
  border-radius: ${constants.sizes.borderRadius};
  :hover {
    transform: scale(0.98);
    border-color: ${constants.colors.sideBarElementForeground}
  }
  @media (max-width: 1200px){
    grid-template-columns: 1fr;
  }
`

function ColorBox(props) {
  const {colorSet, column, row, children} = props;
  const [colorElementArray, setColorElementArray] = useState(<></>);

  useEffect(() => {
    if (colorSet.size){
      const colorArray = Array.from(colorSet)
      const newColorElementArray = colorArray.map(
        (color) => 
          <ColorSegment
            id={`colorSegment-${color}`}
            key={`colorSegment-${color}`}
            color={color}
          />
      )
      setColorElementArray(newColorElementArray)
    } else {
      setColorElementArray([])
    }
  }, [colorSet, column, row])

  return (
    <RelativeContainer
      column={column} 
      row={row}
      id="colorBoxRelativeWrapper"
      key="colorBoxRelativeWrapper"
    >
      <StyledColorsContainer
        id="colorBoxContainer"
        key="colorBoxContainer"
      >
        {colorElementArray}
      </StyledColorsContainer>
        {children}
    </RelativeContainer>
  ) 
}

export default ColorBox;
