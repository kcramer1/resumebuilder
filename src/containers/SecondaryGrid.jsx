import styled from 'styled-components';
import TextBox from '../components/TextBox'
import SelectionButton from '../components/SelectionButton';
import TertiaryGrid from './TertiaryGrid';
import checkIsDark from '../util/checkIsDark';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import constants from '../util/constants.json'
import * as secondaryActType from '../redux/actionTypes/secondary'
import * as tertiaryActType from '../redux/actionTypes/tertiary'
import * as rightSidebarActType from '../redux/actionTypes/rightSidebar'
import * as primaryGridActType from '../redux/actionTypes/primaryGrid'

const actType = { ...secondaryActType, ...tertiaryActType, ...rightSidebarActType, ...primaryGridActType }

const GridContainer = styled.div`
  box-sizing: border-box;
  background-color: ${(props) => props.backgroundColor};
  color: ${(props) => props.fontColor};
  font-family: ${(props) => props.fontFamily.value};
  font-size: ${(props) => props.isSidebarOpen ? `${parseInt(props.fontSize.remSize, 10) * 0.78}rem` : props.fontSize.remSize};
  font-weight: ${(props) => props.isBold? '700' : '400'};
  font-style: ${(props) => props.isItalic? 'italic' : 'normal'};
  text-decoration: ${(props) => props.isUnderlined? 'underline' : 'none'};
  border-style: ${(props) => props.isActive || props.isChildSelected ? 'solid' : 'dashed'};
  border-color: ${props => props.isDark ? '#ffffff' : '#000000'};
  border-width: ${(props) => props.isActive ? '2px' : '1px'};
  ${props => props.modeIndex !== '1' && 'border: none;'}
  height: 100%;
  width: 100%;
  display: grid;
  grid-column: ${props => props.column};
  grid-row: ${props => props.row};
  grid-template-columns: ${props => `repeat(${props.xRepeat}, ${100/props.xRepeat}%) 1%`};
  grid-template-rows: ${props => `repeat(${props.yRepeat}, ${100/props.yRepeat}%) 1%`};
  padding: ${props => `${props.marginArray[1] * 0.5}rem ${props.marginArray[2] * 0.5}rem ${props.marginArray[3] * 0.5}rem ${props.marginArray[0] * 0.5}rem`};
  transition: font-size ${constants.transitionTimes.toggleSidebar};
  ${props => props.isSidebarOpen && 'font-stretch: condensed;'};
  cursor: ${props => (!props.isActive && props.modeIndex === '1') ? 'pointer' : 'default' };
`;

const SecondaryGrid = (props) => {
  const {isActive, xRepeat, yRepeat, column, row, index, backgroundColor, fontColor, fontFamily, fontSize, text, isBold, isItalic, isUnderlined, marginArray, primaryXRepeat, primaryYRepeat} = props;
  const activeTertiaryIndex = useSelector((state) => state.tertiaryGrids.currentGridIndex)
  const tertiaryGrids = useSelector((state) => state.tertiaryGrids.grids)
  const tertiaryBackgroundColor = useSelector((state) => state.rightSidebar.backgroundColor)
  const tertiaryFontColor = useSelector((state) => state.rightSidebar.fontColor)
  const tertiaryFontFamily = useSelector((state) => state.rightSidebar.fontFamily)
  const tertiaryFontSize = useSelector((state) => state.rightSidebar.fontSize)
  const tertiaryIsBold = useSelector((state) => state.rightSidebar.isBold)
  const tertiaryIsItalic= useSelector((state) => state.rightSidebar.isItalic)
  const tertiaryIsUnderlined = useSelector((state) => state.rightSidebar.isUnderlined)
  const tertiaryMarginArray = useSelector((state) => state.rightSidebar.marginArray)
  const isSidebarOpen = useSelector((state) => state.rightSidebar.isOpen)
  const modeIndex = useSelector((state) => state.header.modeIndex);
  const topLeft = useSelector((state) => state.secondaryGrids.topLeft);
  const dispatch = useDispatch()
  const [tertiaryGridElements, setTertiaryGridElements] = useState([])
  // const [topLeft, setTopLeft] = useState([]);
  const [isDark, setIsDark] = useState(false);
  const [gridButtons, setGridButtons] = useState([]);
  
  useEffect(() => {
    setIsDark(checkIsDark(backgroundColor))
  }, [backgroundColor])

  useEffect(() => {
    const gridButtonArray = []
    for (let x = 1; x <= xRepeat + 1; x ++) {
      for (let y = 1; y <= yRepeat + 1; y ++) {
        const isConfirmationButton = !!(topLeft.length && topLeft[0] < x && topLeft[1] < y)
        if (y === 1 && x === xRepeat + 1) {
          gridButtonArray.push(
            <SelectionButton
              id={`selectionButton-secondary-${x}-${y}`}
              key={`selectionButton-secondary-${x}-${y}`}
              column={x}
              row={y}
              layer="secondary"
              iconIndex={2}
              isActive
              isDark={isDark}
              buttonBackgroundColor={constants.colors.delete}
              containerBackgroundColor={backgroundColor}
            />
          )
        } 
        else if (y === yRepeat + 1 && x === 1) {
          gridButtonArray.push(
            <SelectionButton
              id={`selectionButton-secondary-${x}-${y}`}
              key={`selectionButton-secondary-${x}-${y}`}
              column={x}
              row={y}
              layer="secondary"
              iconIndex={3}
              isActive={parseInt(row.split('/')[1], 10) !== primaryYRepeat + 1}
              isDark={isDark}
              buttonBackgroundColor={ constants.colors.addColumnOrRow }
              containerBackgroundColor={backgroundColor}
            />
          )
        } 
        else if (y === yRepeat + 1 && x === xRepeat + 1) {
          if(!topLeft.length) {
            gridButtonArray.push(
              <SelectionButton
                id={`selectionButton-secondary-${x}-${y}`}
                key={`selectionButton-secondary-${x}-${y}`}
                column={x}
                row={y}
                layer="secondary"
                iconIndex={4}
                isActive={parseInt(column.split('/')[1], 10) !== primaryXRepeat + 1}
                isDark={isDark}
                buttonBackgroundColor={constants.colors.addColumnOrRow}
                containerBackgroundColor={backgroundColor}
              />
            )
          } else {
            gridButtonArray.push(
              <SelectionButton
                id={`selectionButton-secondary-${x}-${y}`}
                key={`selectionButton-secondary-${x}-${y}`}
                column={x}
                row={y}
                layer="secondary"
                iconIndex={isConfirmationButton ? 1 : 0}
                isActive={isConfirmationButton || (!topLeft.length && y !== yRepeat + 1)}
                isDark={isDark}
                buttonBackgroundColor={isConfirmationButton ? constants.colors.secondaryConfirmation : constants.colors.secondarySelectorActive }
                containerBackgroundColor={backgroundColor}
              />

            )
          }
        } 
        else {
          gridButtonArray.push(
            <SelectionButton
              id={`selectionButton-secondary-${x}-${y}`}
              key={`selectionButton-secondary-${x}-${y}`}
              column={x}
              row={y}
              layer="secondary"
              iconIndex={ isConfirmationButton ? 1 : 0 }
              isActive={ isConfirmationButton || (!topLeft.length && x !== xRepeat + 1 && y !== yRepeat + 1) }
              isDark={isDark}
              buttonBackgroundColor={
                isConfirmationButton ?
                  constants.colors.secondaryConfirmation :
                  constants.colors.secondarySelectorActive
              }
              containerBackgroundColor={backgroundColor}
            />
          )
        }
      }
    }
    setGridButtons(gridButtonArray)
  },[xRepeat, yRepeat, topLeft, index, backgroundColor, isDark, column, row, primaryXRepeat, primaryYRepeat])

  const deleteContainer = () => {
    dispatch({ type: actType.DELETE_SECONDARY, payload: index })
    dispatch({ type: actType.SET_TEXT, payload: constants.textInputPlaceholder})
    dispatch({ type: actType.SET_IS_BOLD, payload: false })
    dispatch({ type: actType.SET_IS_ITALIC, payload: false })
    dispatch({ type: actType.SET_IS_UNDERLINED, payload: false })
    dispatch({ type: actType.RESET_MARGIN })
    dispatch({ type: actType.SET_FONT_FAMILY, payload: constants.fontFamilyArray[0] })
    dispatch({ type: actType.SET_FONT_SIZE, payload: constants.fontSizeArray[0] })
    dispatch({ type: actType.SET_IS_PRIMARY_ACTIVE, payload: true })
  }

  const setContainerActive = () => {
    if (!isActive){
      dispatch({ type: actType.SET_SECONDARY_TO_INACTIVE })
      dispatch({ type: actType.SET_TEXT, payload: text || constants.textInputPlaceholder })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: true })
      dispatch({ type: actType.SET_COLOR, payload: fontColor })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: false })
      dispatch({ type: actType.SET_COLOR, payload: backgroundColor })
      dispatch({ type: actType.SET_FONT_FAMILY, payload: fontFamily })
      dispatch({ type: actType.SET_FONT_SIZE, payload: fontSize })
      dispatch({ type: actType.SET_IS_BOLD, payload: isBold })
      dispatch({ type: actType.SET_IS_ITALIC, payload: isItalic })
      dispatch({ type: actType.SET_IS_UNDERLINED, payload: isUnderlined })
      dispatch({ type: actType.RESET_MARGIN, payload: marginArray })
      dispatch({ type: actType.SET_SECONDARY_TO_ACTIVE, payload: index })
      dispatch({ type: actType.SET_TERTIARY_TO_INACTIVE })
      dispatch({ type: actType.TRIGGER_TEXT_FOCUS })
      dispatch({ type: actType.SET_IS_PRIMARY_ACTIVE, payload: false })
    }
  }

  const addRow = () => {
    dispatch({ type: actType.ADD_SECONDARY_ROW, payload: index })
  }

  const addColumn = () => {
    dispatch({ type: actType.ADD_SECONDARY_COLUMN, payload: index })
  }

  const handleClick = (e) => {
    if (!isSidebarOpen) {
    }
    if (isActive){
      const id = e.target.id;
      const idArray = id.split('-')
      if (id.includes('delete')) {
        deleteContainer(idArray[idArray.length - 1])
      }
      else if (id.includes('down')) {
        addRow()
      }
      else if (id.includes('right')) {
        addColumn()
      }
      else if(idArray.includes('Button')) {
        const xValue = parseInt(idArray[idArray.length - 2], 10);
        const yValue = parseInt(idArray[idArray.length - 1], 10);
        if (!topLeft.length) {
          dispatch({ type: actType.SET_SECONDARY_TOP_LEFT, payload: [xValue,yValue]})
        } else {
          const xRange = xValue - topLeft[0];
          const yRange = yValue - topLeft[1]; 
          if (xRange > 0 && yRange > 0) {
            const gridProps = 
              {
                column: `${topLeft[0]}/${xValue}`,
                row: `${topLeft[1]}/${yValue}`,
                isActive,
                backgroundColor: tertiaryBackgroundColor,
                fontColor: tertiaryFontColor,
                fontFamily: tertiaryFontFamily,
                fontSize: tertiaryFontSize,
                isBold: tertiaryIsBold,
                isItalic: tertiaryIsItalic,
                isUnderlined: tertiaryIsUnderlined,
                marginArray: tertiaryMarginArray,
                text: ""
              }
            dispatch({ type: actType.RESET_MARGIN })
            dispatch({ type: actType.SET_TERTIARY_TO_INACTIVE })
            dispatch({ type: actType.PERSIST_TERTIARY, payload: {secondaryParentIndex: index, gridProps} })
            dispatch({ type: actType.SET_TEXT, payload: constants.textInputPlaceholder})
            dispatch({ type: actType.SET_SECONDARY_TO_INACTIVE, payload: index })
            dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: false })
            dispatch({ type: actType.SET_SECONDARY_TOP_LEFT, payload: []})
          } else {
            dispatch({ type: actType.SET_SECONDARY_TOP_LEFT, payload: [xValue,yValue]})
          }
        }
      }
    } else {
      setContainerActive()
    }
    e.stopPropagation()
  }

  useEffect(() => {
    if (tertiaryGrids.length) {
      const tertiaryGrid = [...tertiaryGrids[index]];
      if( tertiaryGrid.length){
        const tertiaryGridElementArray = []
        let effectIndex = 0;
        for (const grid of tertiaryGrid) {
          if(grid) {
            tertiaryGridElementArray.push(
              <TertiaryGrid
                id={`tertiaryGrid-${index}-${effectIndex}`}
                key={`tertiaryGrid-${index}-${effectIndex}`}
                secondaryIndex = {index}
                tertiaryIndex={effectIndex}
                {...grid}
              />
            )
          }
          effectIndex ++
        }
        setTertiaryGridElements(tertiaryGridElementArray)
        return
      }
    }
    setTertiaryGridElements([])
  }, [tertiaryGrids, index, dispatch])

  return (
    <>
      <GridContainer
        id={`secondaryGridContainer-${column}-${row}`}
        key={`secondaryGridContainer-${column}-${row}`}
        xRepeat={xRepeat}
        yRepeat={yRepeat}
        column={column}
        row={row}
        onClick={ (e) => handleClick(e) }
        onMouseDown={(e) => {
          e.stopPropagation();
          if (isActive && !e.target.id.includes('Button')) {
            e.preventDefault()
          }
        }}
        backgroundColor={backgroundColor}
        fontColor={fontColor}
        fontFamily={fontFamily}
        fontSize={fontSize}
        isBold={isBold}
        isItalic={isItalic}
        isUnderlined={isUnderlined}
        marginArray={marginArray}
        isActive={isActive}
        isDark={isDark}
        isChildSelected={parseInt(activeTertiaryIndex[0], 10) === index}
        isSidebarOpen={isSidebarOpen}
        modeIndex={modeIndex}
      >
        {
          (!!isActive && modeIndex === '1') && gridButtons     
        }
        <TextBox
          key={`textContainer-${column}-${row}`}
          fontColor={fontColor}
          column={`1/${xRepeat + 1}`}
          row={`1/${yRepeat + 1}`}
          fontFamily={fontFamily}
          fontSize={fontSize}
          fontValue={text}
          isBold={isBold}
          isItalic={isItalic}
          isUnderlined={isUnderlined}
          isSidebarOpen={isSidebarOpen}
          idAndKey={`textContainer-${column}-${row}`}
        />
        {tertiaryGridElements}
      </GridContainer>
    </>
  );
}

export default SecondaryGrid;
