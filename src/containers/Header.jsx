import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled, {keyframes} from 'styled-components';
import constants from '../util/constants.json'
import edit from './icons/edit.svg'
import view from './icons/spectacles.svg'
import iceCream from './icons/iceCream.svg'
import * as secondaryActType from '../redux/actionTypes/secondary'
import * as tertiaryActType from '../redux/actionTypes/tertiary'
import * as rightSidebarActType from '../redux/actionTypes/rightSidebar'
import * as headerActType from '../redux/actionTypes/header'

const actType = { ...secondaryActType, ...tertiaryActType, ...rightSidebarActType, ...headerActType }

const indicatorTransition = (previousModeIndex, column) =>  keyframes`
  0% { opacity: 1; grid-column: ${previousModeIndex};}
  40% { ${previousModeIndex !== column ? 'background-color: inherit;' : 'opacity: 1;'}}
  50% { ${previousModeIndex !== column ? 'opacity: 0; width: 0;' : 'opacity: 1;'}}
  60% { ${previousModeIndex !== column ? 'background-color: inherit;' : 'opacity: 1;'}}
  100% { opacity: 1; grid-column: ${column};}
`;

const HeaderContainer = styled.div`
  background-image: linear-gradient(to right, #000000 , ${constants.colors.sideBarBackground});  /* background-color: ${constants.colors.headerBackground}; */
  position: fixed;
  top: 0;
  left: 0;
  width: ${props => props.isSidebarOpen ? '77%' : '100%'};
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  grid-template-rows: repeat(2, min-content);
  z-index: 90;
  transition: width ${constants.transitionTimes.toggleSidebar} ease;
`;

const ButtonText = styled.h2`
  margin: 0;
  padding: 0;
  color: #ffffff;
  font-size: ${constants.sizes.headerInputText};
`;

const ModesBar = styled.div`
  position: relative;
  background-color: inherit;
  display: grid;
  grid-template-columns: repeat(3, min-content);
  grid-template-rows: repeat(2, min-content);
  justify-content: space-between;
  align-content: end;
  grid-column: 2/5;
  grid-row: 2/3;
  padding-top: 1rem;
  padding-bottom: 0.2rem;
  grid-row-gap: 0.5rem;
`;

const ModeButton = styled.button`
  margin: 0;
  padding: 0;
  font-size: 2rem;
  min-width: 6rem;
  grid-column: ${props => props.column};
  grid-row: 1;
  border: none;
  background-color: inherit;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-items: center;
  align-items: center;
`;

const ModeIndicator = styled.div`
  position: relative;
  height: 0.6rem;
  background-color: #ffffff;
  width: 6rem;
  border-radius: ${constants.sizes.borderRadius};
  grid-row: 2;
  align-self: end;
  justify-self: center;
  animation: ${props => indicatorTransition(props.previousModeIndex, props.column)};
  animation-duration: 200ms;
  animation-fill-mode: forwards;
`;

const ModeIcon = styled.img`
  margin-top: 0.3rem;
  height: ${constants.sizes.headerInputElement};
`;

const modes = [{value: 'EDIT', icon: edit, position: 1}, {value: 'VIEW', icon: view, position: 2}, {value: 'ALA', icon: iceCream, position: 3}];

function Header() {
  const isOpen = useSelector((state) => state.rightSidebar.isOpen)
  const [previousModeIndex, setPreviousModeIndex] = useState(1)
  const modeIndex = useSelector((state) => state.header.modeIndex)
  const dispatch = useDispatch()

  const handleClick = (e) => {
    const idArray = e.target.id.split('-');
    if (idArray.length > 1) {
      const index = idArray[idArray.length -1];
      if (index === '1') {
        dispatch({ type: actType.SET_IS_SIDEBAR_OPEN, payload: true })
      } else {
        dispatch({ type: actType.SET_IS_SIDEBAR_OPEN, payload: false })
      }
      if (index === '3') {
        dispatch({ type: actType.SET_MODAL_IS_VISIBLE, payload: true })
      }
      dispatch({type: actType.SET_MODE_INDEX, payload: idArray[idArray.length -1] })
    }
  }

  useEffect(() => {
    setPreviousModeIndex(modeIndex)
  }, [modeIndex])

  return (
    <HeaderContainer id="headerContainer" key="headerContainer" isSidebarOpen={isOpen}>
      <ModesBar 
        id="modesBar"
        onClick={(e) => handleClick(e)}
      >
        {modes.map(mode => 
          <ModeButton 
            id={`modeButton-${mode.position}`} 
            key={`modeButton-${mode.position}`} 
            column={mode.position}
          >
            <ButtonText
              id={`modeButtonText-${mode.position}`} 
              key={`modeButtonText-${mode.position}`} 
            >
              {mode.value}
            </ButtonText>
            <ModeIcon 
              src={mode.icon} 
              id={`modeButtonIcon-${mode.position}`} 
              key={`modeButtonIcon-${mode.position}`} 
            />
          </ModeButton>
          )
        }
      <ModeIndicator column={modeIndex} previousModeIndex={previousModeIndex} />
      </ModesBar> 
    </HeaderContainer>
  );
}

export default Header;
