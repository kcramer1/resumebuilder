import TutorialPopup from '../components/TutorialPopup';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import * as secondaryActType from '../redux/actionTypes/secondary'
import * as tertiaryActType from '../redux/actionTypes/tertiary'
import * as rightSidebarActType from '../redux/actionTypes/rightSidebar'
import * as primaryGridActType from '../redux/actionTypes/primaryGrid'
import * as headerActType from '../redux/actionTypes/header'
import * as tutorialActType from '../redux/actionTypes/tutorial'

const actType = { ...secondaryActType, ...tertiaryActType, ...rightSidebarActType, ...primaryGridActType, ...headerActType, ...tutorialActType }


function Tutorial(props) {
  const tutorialIndex = useSelector((state) => state.tutorial.tutorialIndex);
  const primaryTopLeftToggle = useSelector((state) => state.tutorial.primaryTopLeftToggle);
  const persistSecondaryGridToggle = useSelector((state) => state.tutorial.persistSecondaryGridToggle);
  const secondaryTopLeftToggle = useSelector((state) => state.tutorial.secondaryTopLeftToggle);
  const persistTertiaryGridToggle = useSelector((state) => state.tutorial.persistTertiaryGridToggle);
  const setColorToggle = useSelector((state) => state.tutorial.setColorToggle);
  const setIsTargetColorFontToggle = useSelector((state) => state.tutorial.setIsTargetColorFontToggle);
  const setTextToggle = useSelector((state) => state.tutorial.setTextToggle);
  const setFontFamilyToggle = useSelector((state) => state.tutorial.setFontFamilyToggle);
  const setFontSizeToggle = useSelector((state) => state.tutorial.setFontSizeToggle);
  const setIsPrimaryActiveToggle = useSelector((state) => state.tutorial.setIsPrimaryActiveToggle);
  const setSecondaryToActiveToggle = useSelector((state) => state.tutorial.setSecondaryToActiveToggle);
  const selectViewToggle = useSelector((state) => state.tutorial.selectViewToggle);
  const selectAlaToggle = useSelector((state) => state.tutorial.selectAlaToggle);
  const dispatch = useDispatch();
  const tutorialArray = [
    {
      instruction: 'Would you like to view a short tutorial?  Click "Next" to view the tutorial or click "exit" at any time to leave the tutorial.',
      continueEvent: null,
      shouldDisplayNext: true
    },
    {
      instruction: 'To define a container, first select a "plus" icon.  Select a "Plus" icon to continue the tutorial.',
      continueEvent: primaryTopLeftToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'To finish defining a container, we need to select a "Check" icon.  Select a "Check" icon to continue the tutorial.',
      continueEvent: persistSecondaryGridToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'When you define a container it is automatically selected.  We can unselect this container by clicking on the white space around it.  Click on the white space to continue. ',
      continueEvent: setIsPrimaryActiveToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'We can reselect the container that we defined by clicking on it.  Click on the container that we defined earlier to continue. ',
      continueEvent: setSecondaryToActiveToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'Now that the container is selected, we can edit its background color.  Select a color from the color picker at the top right of the screen to continue. ',
      continueEvent: setColorToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'Good!  You can also add text to a container.  Type a five letter word in the text input on the right side of the screen to continue the tutorial.',
      continueEvent: setTextToggle,
      shouldDisplayNext: false
    },
    {
      instruction: "We can change the font color too.  Toggle the switch on the right sidebar below the color picker to continue.",
      continueEvent: setIsTargetColorFontToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'Now we can set the font color!  Select a font color from the color picker on the top right portion of the screen to continue.',
      continueEvent: setColorToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'We can also select a container within a container.  Select a "Plus" icon within the container that we have defined to continue.',
      continueEvent: secondaryTopLeftToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'To finish defining this container, select a "Check" icon like we did to define the first container.',
      continueEvent: persistTertiaryGridToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'Good!  Now we can define a color for this container.  Select a color to continue.',
      continueEvent: setColorToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'You can also add text to this container.  Type another five letter word in the text box to continue.',
      continueEvent: setTextToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'We can also change the font type for our text.  Change the font type from Roboto to a font family of your choice on the dropdown on the right sidebar',
      continueEvent: setFontFamilyToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'Great!  We can change the font size as well!  Change the font size from XLarge to a font size of your choice to continue.',
      continueEvent: setFontSizeToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'At any time in the design process, you can view your work without the editing interface.  Click the "View" button in the header to continue.',
      continueEvent: selectViewToggle,
      shouldDisplayNext: false
    },
    {
      instruction: 'You can replace your work with a visual representation of an ice cream cone.  Click the "ALA" button on the header to continue.',
      continueEvent: selectAlaToggle,
      shouldDisplayNext: false,
      additionalDispatch: [{type: actType.DEPLOY_SECONDARY_ICE_CREAM}, {type: actType.DEPLOY_TERTIARY_ICE_CREAM}, { type: actType.SET_MODE_INDEX, payload: '2' } ]
    },
    {
      instruction: 'You can click the "EDIT" button if you want to change the flavor and to explore additional functionality! Press exit to close this tutorial.',
      continueEvent: primaryTopLeftToggle,
      shouldDisplayNext: false
    }
  ]

  const currentCondition = tutorialArray[tutorialIndex].continueEvent;

  useEffect(() => {
    dispatch({ type: actType.INCREASE_TUTORIAL_INDEX, payload: currentCondition })
    dispatch({ type: actType.RESET_TOGGLES})
  }, [currentCondition, dispatch])

  return (
    <TutorialPopup instruction={tutorialArray[tutorialIndex].instruction} shouldDisplayNext={tutorialArray[tutorialIndex].shouldDisplayNext} />
  );
}

export default Tutorial;
