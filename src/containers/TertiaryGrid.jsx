import styled from 'styled-components';
import TextBox from '../components/TextBox'
import SelectionButton from '../components/SelectionButton';
import checkIsDark from '../util/checkIsDark';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import constants from '../util/constants.json'
import * as tertiaryActType from '../redux/actionTypes/tertiary'
import * as secondaryActType from '../redux/actionTypes/secondary'
import * as rightSidebarActType from '../redux/actionTypes/rightSidebar'
import * as primaryGridActType from '../redux/actionTypes/primaryGrid'

const actType = { ...tertiaryActType, ...secondaryActType, ...rightSidebarActType, ...primaryGridActType }

const GridContainer = styled.div`
  display: grid;
  position: relative;
  box-sizing: border-box;
  background-color: ${(props) => props.backgroundColor};
  color: ${(props) => props.fontColor};
  font-family: ${(props) => props.fontFamily.value};
  font-size: ${(props) => props.isSidebarOpen ? `${parseInt(props.fontSize.remSize, 10) * 0.78}rem` : props.fontSize.remSize};
  font-weight: ${(props) => props.isBold? '700' : '400'};
  font-style: ${(props) => props.isItalic? 'italic' : 'normal'};
  text-decoration: ${(props) => props.isUnderlined? 'underline' : 'none'};
  border-style: ${(props) => props.isActive ? 'solid' : 'dashed'};
  border-color: ${props => props.isDark ? '#ffffff' : '#000000'};
  border-width: ${(props) => props.isActive ? '2px' : '1px'};
  ${props => props.modeIndex !== '1' && 'border: none;'}
  height: 100%;
  width: 100%;
  grid-column: ${props => props.column};
  grid-row: ${props => props.row};
  grid-template-columns: 100%;
  grid-template-rows: 100%;
  padding: ${props => `${props.marginArray[1] * 0.5}rem ${props.marginArray[2] * 0.5}rem ${props.marginArray[3] * 0.5}rem ${props.marginArray[0] * 0.5}rem`};
  cursor: ${props => (!props.isActive && props.modeIndex === '1') ? 'pointer' : 'default' };
`;

const TertiaryGrid = (props) => {
  const {isActive, column, row, backgroundColor, fontColor, fontFamily, fontSize, text, isBold, isItalic, isUnderlined, marginArray, secondaryIndex, tertiaryIndex} = props;
  const isSidebarOpen = useSelector((state) => state.rightSidebar.isOpen)
  const secondaryParent = useSelector((state) => state.secondaryGrids).grids[secondaryIndex]
  const modeIndex = useSelector((state) => state.header.modeIndex);
  const [isDark, setIsDark] = useState(false)
  const dispatch = useDispatch()

  useEffect(() => {
    setIsDark(checkIsDark(backgroundColor))
  }, [backgroundColor])

  const setContainerActive = (e) => {
    if (!isActive) {
      const idArray = e.target.id.split('-')
      const secondaryContainerIndex = idArray[idArray.length - 2]
      const tertiaryContainerIndex = idArray[idArray.length - 1]
      dispatch({ type: actType.SET_SECONDARY_TO_INACTIVE })
      dispatch({ type: actType.SET_TERTIARY_TO_INACTIVE })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: false })
      dispatch({ type: actType.SET_COLOR, payload: backgroundColor })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: true })
      dispatch({ type: actType.SET_COLOR, payload: fontColor })
      dispatch({ type: actType.SET_TEXT, payload: text || constants.textInputPlaceholder })
      dispatch({ type: actType.SET_FONT_FAMILY, payload: fontFamily })
      dispatch({ type: actType.SET_FONT_SIZE, payload: fontSize })
      dispatch({ type: actType.SET_TEXT, payload: text || constants.textInputPlaceholder })
      dispatch({ type: actType.SET_IS_BOLD, payload: isBold })
      dispatch({ type: actType.SET_IS_ITALIC, payload: isItalic })
      dispatch({ type: actType.SET_IS_UNDERLINED, payload: isUnderlined })
      dispatch({ type: actType.RESET_MARGIN, payload: marginArray })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: false })
      dispatch({ type: actType.SET_TERTIARY_TO_ACTIVE, payload: [secondaryContainerIndex, tertiaryContainerIndex] })
      dispatch({ type: actType.TRIGGER_TEXT_FOCUS })
      dispatch({type: actType.SET_IS_PRIMARY_ACTIVE, payload: false})
    }
  }
  
  const handleClick = (e) => {
    const id = e.target.id;
    if (id.includes('delete')) {
      dispatch({ type: actType.DELETE_TERTIARY, payload: [secondaryIndex, tertiaryIndex]})
      dispatch({ type: actType.SET_TERTIARY_TO_INACTIVE, payload: [secondaryIndex, tertiaryIndex]})
      dispatch({ type: actType.SET_TEXT, payload: secondaryParent.text || constants.textInputPlaceholder })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: true })
      dispatch({ type: actType.SET_COLOR, payload: secondaryParent.fontColor })
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: false })
      dispatch({ type: actType.SET_COLOR, payload: secondaryParent.backgroundColor })
      dispatch({ type: actType.SET_FONT_FAMILY, payload: secondaryParent.fontFamily })
      dispatch({ type: actType.SET_FONT_SIZE, payload: secondaryParent.fontSize })
      dispatch({ type: actType.SET_IS_BOLD, payload: secondaryParent.isBold })
      dispatch({ type: actType.SET_IS_ITALIC, payload: secondaryParent.isItalic })
      dispatch({ type: actType.SET_IS_UNDERLINED, payload: secondaryParent.isUnderlined })
      dispatch({ type: actType.RESET_MARGIN, payload: secondaryParent.marginArray })
      dispatch({ type: actType.SET_SECONDARY_TO_ACTIVE, payload: secondaryIndex })
    } else {
      setContainerActive(e)
    } 
    e.stopPropagation()
  }

  return (
    <>
      <GridContainer
        id={`tertiaryGridContainer-${secondaryIndex}-${tertiaryIndex}`}
        key={`tertiaryGridContainer-${secondaryIndex}-${tertiaryIndex}`}
        column={column}
        row={row}
        onClick={ (e) => handleClick(e) }
        onMouseDown={ (e) => {
          e.stopPropagation()
          if (isActive) {
            e.preventDefault()
          }
        } }
        backgroundColor={backgroundColor}
        fontColor={fontColor}
        fontFamily={fontFamily}
        fontSize={fontSize}
        isBold={isBold}
        isItalic={isItalic}
        isUnderlined={isUnderlined}
        marginArray={marginArray}
        isActive={isActive}
        isDark={isDark}
        modeIndex={modeIndex}
      >
        {isActive && modeIndex === '1' && <SelectionButton
          id={`deleteButton-tertiary-${secondaryIndex}-${tertiaryIndex}`}
          key={`deleteButton-tertiary-${secondaryIndex}-${tertiaryIndex}`}
          layer="tertiary"
          column={2}
          row={1}
          iconIndex={2}
          isActive
          isDark={isDark}
          buttonBackgroundColor={constants.colors.delete}
          containerBackgroundColor={backgroundColor}
        />}
        <TextBox
          fontColor={fontColor}
          fontFamily={fontFamily}
          fontSize={fontSize}
          fontValue={text}
          isBold={isBold}
          isItalic={isItalic}
          isUnderlined={isUnderlined}
          isSidebarOpen={isSidebarOpen}
          idAndKey={`textContainer-${secondaryIndex}-${tertiaryIndex}`}
        />
      </GridContainer>
    </>
  );
}

export default TertiaryGrid;
