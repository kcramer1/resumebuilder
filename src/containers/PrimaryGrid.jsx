import styled from 'styled-components';
import SecondaryGrid from './SecondaryGrid';
import Popup from './Popup';
import Tutorial from './Tutorial';
import SelectionButton from '../components/SelectionButton';
import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import constants from '../util/constants.json'
import * as secondaryActType from '../redux/actionTypes/secondary'
import * as tertiaryActType from '../redux/actionTypes/tertiary'
import * as rightSidebarActType from '../redux/actionTypes/rightSidebar'
import * as primaryGridActType from '../redux/actionTypes/primaryGrid'

const actType = { ...secondaryActType, ...tertiaryActType, ...rightSidebarActType, ...primaryGridActType }

const GridContainer = styled.div`
  height: 100%;
  margin-top: 8rem;
  width: ${(props) => props.isSidebarOpen ? '75%' : '95%'};
  background-color: white;
  display: grid;
  grid-template-columns: ${props => `repeat(${props.xRepeat}, ${100/props.xRepeat}%) 1%`};
  grid-template-rows: ${props => `repeat(${props.yRepeat}, ${100/props.yRepeat}%) 1%`};
  transition: width ${constants.transitionTimes.toggleSidebar};
  cursor: ${props => props.isActive ? 'default' : 'pointer' };
`;

const backgroundColor = constants.colors.primaryBackgroundColor;

let xRepeat = 16;
let yRepeat = 16;

function PrimaryGrid() {
  const isSidebarOpen = useSelector((state) => state.rightSidebar.isOpen)
  const secondaryGrids = useSelector((state) => state.secondaryGrids.grids)
  const secondaryBackgroundColor = useSelector((state) => state.rightSidebar.backgroundColor)
  const secondaryFontColor = useSelector((state) => state.rightSidebar.fontColor)
  const secondaryFontFamily = useSelector((state) => state.rightSidebar.fontFamily)
  const secondaryFontSize = useSelector((state) => state.rightSidebar.fontSize)
  const secondaryIsBold = useSelector((state) => state.rightSidebar.isBold)
  const secondaryIsItalic= useSelector((state) => state.rightSidebar.isItalic)
  const secondaryIsUnderlined = useSelector((state) => state.rightSidebar.isUnderlined)
  const secondaryMarginArray = useSelector((state) => state.rightSidebar.marginArray)
  const isModalVisible = useSelector((state) => state.header.isModalVisible)
  const modeIndex = useSelector((state) => state.header.modeIndex);
  const tutorialIndex = useSelector((state) => state.tutorial.tutorialIndex);
  const topLeft = useSelector((state) => state.primaryGrid.topLeft);
  const isActive = useSelector((state) => state.primaryGrid.isActive);
  const dispatch = useDispatch()
  const [gridButtons, setSecondaryGridButtons] = useState([])
  const [secondaryGridElements, setSecondaryGridElements] = useState([])

  useEffect(() => {
    const gridButtonArray = []
    for (let x = 1; x <= xRepeat + 1; x ++) {
      for (let y = 1; y <= yRepeat + 1; y ++) {
        gridButtonArray.push(
          !!(topLeft.length && topLeft[0] < x && topLeft[1] < y) ?
            <SelectionButton
              id={`selectionButton-primary-${x}-${y}`}
              key={`selectionButton-primary-${x}-${y}`}
              column={x}
              row={y}
              layer="primary"
              iconIndex={1}
              isActive
              isDark={false}
              buttonBackgroundColor={ constants.colors.primaryConfirmation }
              containerBackgroundColor={backgroundColor}
            /> 
            :
            <SelectionButton
              id={`selectionButton-primary-${x}-${y}`}
              key={`selectionButton-primary-${x}-${y}`}
              column={x}
              row={y}
              layer="primary"
              iconIndex={0}
              isActive={x !== xRepeat + 1 && y !== yRepeat + 1 && !topLeft.length }
              isDark={false}
              buttonBackgroundColor={ constants.colors.primarySelectorActive }
              containerBackgroundColor={backgroundColor}
            />
        )
      }
    }
    setSecondaryGridButtons(gridButtonArray)
  },[topLeft])

  useEffect(() => {
    const secondaryGridElementArray = []
    let index = 0;
    for (const grid of secondaryGrids) {
      if(grid) {
        secondaryGridElementArray.push(
          <SecondaryGrid
            id={`secondaryGrid-${index}`}
            key={`secondaryGrid-${index}`}
            index = {index}
            {...grid}
          />
        )
      }
      index ++
      setSecondaryGridElements(secondaryGridElementArray)
    }
  }, [secondaryGrids, dispatch])

  const handleClick = (e) => {
    if (isActive) {
      const idArray = e.target.id.split('-')
      const xValue = parseInt(idArray[idArray.length - 2], 10);
      const yValue = parseInt(idArray[idArray.length - 1], 10);
      if(Number.isInteger(xValue) && Number.isInteger(yValue) && idArray[0].includes('primary')) {
        if (!topLeft.length) {
          dispatch({ type: actType.SET_PRIMARY_TOP_LEFT, payload: [xValue,yValue]})
          // setTopLeft([xValue,yValue])
        } else {
          const xRange = xValue - topLeft[0];
          const yRange = yValue - topLeft[1]; 
          if (xRange > 0 && yRange > 0) {
            const payload = 
              {
                xRepeat: xRange * 2,
                yRepeat: yRange * 2,
                column: `${topLeft[0]}/${xValue}`,
                row: `${topLeft[1]}/${yValue}`,
                isActive: true,
                backgroundColor: secondaryBackgroundColor,
                fontColor: secondaryFontColor,
                fontFamily: secondaryFontFamily,
                fontSize: secondaryFontSize,
                isBold: secondaryIsBold,
                isItalic: secondaryIsItalic,
                isUnderlined: secondaryIsUnderlined,
                marginArray: secondaryMarginArray,
                primaryXRepeat: xRepeat,
                primaryYRepeat: yRepeat
              }
            dispatch({ type: actType.PERSIST_SECONDARY, payload })
            dispatch({ type: actType.ADD_TERTIARY_PLACEHOLDER })
            dispatch({ type: actType.TRIGGER_TEXT_FOCUS })
            dispatch({ type: actType.SET_PRIMARY_TOP_LEFT, payload: []})
            dispatch({ type: actType.SET_IS_PRIMARY_ACTIVE, payload: false })
          } else {
            dispatch({ type: actType.SET_PRIMARY_TOP_LEFT, payload: [xValue,yValue]})

            // setTopLeft([xValue,yValue])
          }
        }
      }
    } else {
      dispatch({ type: actType.SET_SECONDARY_TO_INACTIVE })
      dispatch({ type: actType.SET_TERTIARY_TO_INACTIVE })
      dispatch({ type: actType.SET_TEXT, payload: constants.textInputPlaceholder})
      dispatch({ type: actType.SET_FONT_FAMILY, payload: constants.fontFamilyArray[0]})
      dispatch({ type: actType.SET_FONT_SIZE, payload: constants.fontSizeArray[0]})
      dispatch({ type: actType.SET_IS_BOLD, payload: false})
      dispatch({ type: actType.SET_IS_ITALIC, payload: false})
      dispatch({ type: actType.SET_IS_UNDERLINED, payload: false})
      dispatch({ type: actType.SET_IS_COLOR_TARGET_FONT, payload: false})
      dispatch({ type: actType.RESET_MARGIN})
      dispatch({ type: actType.SET_IS_PRIMARY_ACTIVE, payload: true })
    }
  }

  return (
    <GridContainer
      id="primaryGrid"
      key="primaryGrid"
      xRepeat={xRepeat}
      yRepeat={yRepeat}
      isActive={!isSidebarOpen}
      isSidebarOpen={isSidebarOpen}
      onClick={ (e) => handleClick(e) }
    >
      {(modeIndex === '1' && isActive) && gridButtons}
      {secondaryGridElements}
      {isModalVisible && <Popup />}
      {tutorialIndex >= 0 && <Tutorial />}
    </GridContainer>
  );
}

export default PrimaryGrid;
