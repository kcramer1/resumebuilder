import './RightSidebar.css'
import { useEffect, useRef } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';
import CheckBox from '../../components/CheckBox';
import ColorBox from '../../components/ColorBox';
import ButtonBoxLabel from '../../components/ButtonBoxLabel';
import IndentButton from '../../components/IndentButton';
import Switch from '../../components/Switch';
import Dropdown from '../../components/Dropdown';
import { HexColorPicker } from "react-colorful";
import constants from '../../util/constants.json'
import * as secondaryActType from '../../redux/actionTypes/secondary'
import * as tertiaryActType from '../../redux/actionTypes/tertiary'
import * as rightSidebarActType from '../../redux/actionTypes/rightSidebar'

const actType = { ...secondaryActType, ...tertiaryActType, ...rightSidebarActType }

const SideBarContainer = styled.div`
  position: fixed;
  top: 0;
  right: 0%;
  width: 4%;
  width: ${(props) => props.isOpen? '23%' : '1%'};
  height: 100vh;
  background-color: ${constants.colors.sideBarBackground};
  transition: all ${constants.transitionTimes.toggleSidebar} ease;
  z-index: 100;
  ${props => props.isOpen && 'min-width: 120px;'}
`;

const GridContainer = styled.div`
  visibility: ${props => props.isOpen? "visible" : "hidden"};
  opacity: ${props => props.isOpen? 1 : 0};
  width: 100%;
  height: 100%;
  background-color: inherit;
  display: grid;
  grid-column-gap: 2%;
  grid-row-gap: 1.5rem;
  grid-template-columns: 1rem repeat(3, 1fr) 1rem;
  grid-template-rows: 1rem 1fr min-content min-content min-content 1fr 17.5rem 1rem;
  transition: all ${constants.transitionTimes.toggleSidebar};
  @media (max-height: ${constants.screenDimensionBreakpoints.verticalThird}){
    grid-template-rows: 1rem 1fr min-content min-content min-content 1fr 1rem;
  }
`;

const TextInput = styled.textarea`
  color: ${constants.colors.sideBarElementForeground};
  font-size: ${constants.sizes.sidebarInputText};
  grid-column: ${props => props.column};
  grid-row: ${props => props.row};
  background-color: ${constants.colors.sideBarElementBackground};
  border-radius: ${constants.sizes.borderRadius};
  border: none;
  overflow: auto;
  outline: none;
  padding: 6px;
  :hover {
    border: 3px solid ${constants.colors.sideBarElementForeground}
  }
  :focus{
    border: 4px solid ${constants.colors.sideBarElementForeground}
  }
`;

const CheckBoxesContainer = styled.div`
  grid-row: 5;
  grid-column: 2/5;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: min-content;
  grid-column-gap: 2%;
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalFirst}){
    display: flex;
    flex-direction: column;
    grid-column: 2;
  }
`;

const DropdownContainer = styled.div`
  grid-row: 4;
  grid-column: 2/5;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-rows: min-content;
  grid-gap: 1rem;
  @media (max-width: ${constants.screenDimensionBreakpoints.horizontalFirst}){
    grid-template-columns: 1fr;
  }
`;

const TextIndentContainer = styled.div`
  position: relative;
  grid-column: 2;
  grid-row: ${props => props.row};
  display: grid;
  grid-template-columns: repeat(3, auto);
  grid-template-rows: repeat(3, min-content);
  grid-gap: 1rem;
  padding: 1rem;
  padding-bottom: 2rem;
  border: 3px solid ${constants.colors.sideBarElementBackground};
  border-radius: ${constants.sizes.borderRadius};
  :hover {
    transform: scale(0.98);
    border-color: ${constants.colors.sideBarElementForeground}
  }
  transition: transform 100ms;
  @media (max-height: ${constants.screenDimensionBreakpoints.verticalThird}){
    opacity: 0;
    display: none;
  }
`;

function RightSidebar() {
  const isOpen = useSelector((state) => state.rightSidebar.isOpen)
  const sidebarBackgroundColor = useSelector((state) => state.rightSidebar.backgroundColor)
  const sidebarFontColor = useSelector((state) => state.rightSidebar.fontColor)
  const sidebarText = useSelector((state) => state.rightSidebar.enteredText)
  const sidebarFontFamily = useSelector((state) => state.rightSidebar.fontFamily)
  const sidebarFontSize = useSelector((state) => state.rightSidebar.fontSize)
  const sidebarIsBold = useSelector((state) => state.rightSidebar.isBold)
  const sidebarIsItalic = useSelector((state) => state.rightSidebar.isItalic)
  const sidebarIsUnderlined = useSelector((state) => state.rightSidebar.isUnderlined)
  const activeSecondaryIndex = useSelector((state) => state.secondaryGrids.currentGridIndex)
  const activeTertiaryIndex = useSelector((state) => state.tertiaryGrids.currentGridIndex)
  const isTargetColorFont = useSelector((state) => state.rightSidebar.isTargetColorFont)
  const marginArray = useSelector((state) => state.rightSidebar.marginArray)
  const textFocusTrigger = useSelector((state) => state.rightSidebar.textFocusTrigger)
  const secondaryGrids = useSelector((state) => state.secondaryGrids).grids
  const tertiaryGrids = useSelector((state) => state.tertiaryGrids).grids
  const utilizedColors = useSelector((state) => state.rightSidebar).utilizedColors
  const dispatch = useDispatch()
  const ref = useRef(null)

  useEffect(() => {
    if (ref && ref.current) {
      ref.current.focus()
    }
    if (ref && ref.current && document.activeElement !== ref.current) {
      setTimeout(() => ref.current.focus(), 100)
    }
  }, [ref, textFocusTrigger])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({type: actType.SET_SECONDARY_MARGIN, payload: {secondaryIndex: activeSecondaryIndex, value: marginArray}})
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({type: actType.SET_TERTIARY_MARGIN, payload: {tertiaryIndex: activeTertiaryIndex, value: marginArray}})
    }
  },[marginArray, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({type: actType.SET_SECONDARY_FONT_FAMILY, payload: {secondaryIndex: activeSecondaryIndex, value: sidebarFontFamily}})
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({type: actType.SET_TERTIARY_FONT_FAMILY, payload: {tertiaryIndex: activeTertiaryIndex, value: sidebarFontFamily}})
    }
  },[sidebarFontFamily, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({type: actType.SET_SECONDARY_FONT_SIZE, payload: {secondaryIndex: activeSecondaryIndex, value: sidebarFontSize}})
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({type: actType.SET_TERTIARY_FONT_SIZE, payload: {tertiaryIndex: activeTertiaryIndex, value: sidebarFontSize}})
    }
  },[sidebarFontSize, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({type: actType.SET_SECONDARY_IS_ITALIC, payload: {secondaryIndex: activeSecondaryIndex, value: sidebarIsItalic}})
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({type: actType.SET_TERTIARY_IS_ITALIC, payload: {tertiaryIndex: activeTertiaryIndex, value: sidebarIsItalic}})
    }
  },[sidebarIsItalic, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({type: actType.SET_SECONDARY_IS_UNDERLINED, payload: {secondaryIndex: activeSecondaryIndex, value: sidebarIsUnderlined}})
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({type: actType.SET_TERTIARY_IS_UNDERLINED, payload: {tertiaryIndex: activeTertiaryIndex, value: sidebarIsUnderlined}})
    }
  },[sidebarIsUnderlined, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({type: actType.SET_SECONDARY_IS_BOLD, payload: {secondaryIndex: activeSecondaryIndex, value: sidebarIsBold}})
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({type: actType.SET_TERTIARY_IS_BOLD, payload: {tertiaryIndex: activeTertiaryIndex, value: sidebarIsBold}})
    }
  },[sidebarIsBold, activeSecondaryIndex, activeTertiaryIndex, dispatch])
  
  useEffect(() => {
    dispatch({type: actType.SET_UTILIZED_COLORS, payload: {secondaryGrids, tertiaryGrids}})
  },[secondaryGrids, tertiaryGrids, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({
        type: actType.SET_SECONDARY_BACKGROUND_COLOR,
        payload: {
          secondaryIndex: activeSecondaryIndex, 
          value: sidebarBackgroundColor
        }
      })
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({
        type: actType.SET_TERTIARY_BACKGROUND_COLOR,
        payload: {
          tertiaryIndex: activeTertiaryIndex, 
          value: sidebarBackgroundColor
        }
      })
    }
  },[sidebarBackgroundColor, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if (sidebarText !== constants.textInputPlaceholder) {
      if(activeSecondaryIndex >= 0) {
        dispatch({
          type: actType.SET_SECONDARY_TEXT,
          payload: {
            value: sidebarText, 
            secondaryIndex: activeSecondaryIndex
          }
        })
      }
      else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
        dispatch({
          type: actType.SET_TERTIARY_TEXT,
          payload: {
            value: sidebarText,
            tertiaryIndex: activeTertiaryIndex
          }
        })
      }
    }
  },[sidebarText, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  useEffect(() => {
    if(activeSecondaryIndex >= 0) {
      dispatch({
        type: actType.SET_SECONDARY_FONT_COLOR,
        payload: {
          secondaryIndex: activeSecondaryIndex, 
          value: sidebarFontColor
        }
      })
    }
    else if(activeTertiaryIndex[0] >= 0 && activeTertiaryIndex[1] >= 0) {
      dispatch({
        type: actType.SET_TERTIARY_FONT_COLOR,
        payload: {
          tertiaryIndex: activeTertiaryIndex, 
          value: sidebarFontColor
        }
      })
    }
  },[sidebarFontColor, activeSecondaryIndex, activeTertiaryIndex, dispatch])

  return (
    <SideBarContainer 
      id="sideBarContainer"
      isOpen={isOpen}
      className="RightSidebar"
    >
      <GridContainer id="sidebarGridContainer" isOpen={isOpen}>
        <HexColorPicker
          color={isTargetColorFont ? sidebarFontColor : sidebarBackgroundColor} 
          onChange={
            (e) => {
              dispatch({ type: actType.SET_COLOR, payload: e })
            }
          } 
          style={{ gridColumn: "2/5", gridRow: 2, width: "100%", height: "100%" }} 
        />
        <Switch
          uncheckedColor={constants.colors.sideBarElementBackground} 
          checkedColor={constants.colors.sideBarElementBackground} 
          column="2/5"
          row={3}
          rightLabelText="Font"
          rightFontColor={constants.colors.sideBarElementBackground}
          leftLabelText="Background"
          leftFontColor={constants.colors.sideBarElementBackground}
          remWidth={4}
        />
        <DropdownContainer id="dropdownSidebarContainer">
          <Dropdown
            selectionArray={constants.fontFamilyArray}
            column="2/4"
            row={5}
            isFontFamily
          />
          <Dropdown
            selectionArray={constants.fontSizeArray}
            column="4"
            row={5}
            isFontFamily={false}
          />
        </DropdownContainer>
        <CheckBoxesContainer id="sidebarCheckBoxesContainer">
          <CheckBox
            uncheckedColor={constants.colors.sideBarElementBackground} 
            checkedColor={constants.colors.sideBarElementForeground} 
            checkmarkColor={constants.colors.sideBarElementBackground}
            column={2}
            row={1}
            labelTextIndex={0}
            fontColor={constants.colors.sideBarElementBackground}
          />
          <CheckBox
            uncheckedColor={constants.colors.sideBarElementBackground} 
            checkedColor={constants.colors.sideBarElementForeground} 
            checkmarkColor={constants.colors.sideBarElementBackground}
            column={3}
            row={1}
            labelTextIndex={1}
            fontColor={constants.colors.sideBarElementBackground}
          />
          <CheckBox
            uncheckedColor={constants.colors.sideBarElementBackground} 
            checkedColor={constants.colors.sideBarElementForeground} 
            checkmarkColor={constants.colors.sideBarElementBackground}
            column={4}
            row={1}
            labelTextIndex={2}
            fontColor={constants.colors.sideBarElementBackground}
          />
        </CheckBoxesContainer>
        <TextInput
          id="sidebarTextInput"
          ref={ref}
          type="text" 
          value={sidebarText}
          column={'2/5'}
          row={6}
          onBlur={(e) => {
            if (e.target.value === '') {
              dispatch(
                { 
                  type: actType.SET_TEXT, 
                  payload: constants.textInputPlaceholder
                }
              )
            } 
          }}
          onFocus={(e) => {
            if (e.target.value === constants.textInputPlaceholder) {
              dispatch(
                { 
                  type: actType.SET_TEXT, 
                  payload: ""
                }
              )
            } 
          }}
          onChange={
            (e) => {
              dispatch(
                { 
                  type: actType.SET_TEXT,
                  payload: e.target.value
                }
              )
            }
          }
        />
        <ColorBox column={'3/5'} row={7} colorSet={utilizedColors} >
          <ButtonBoxLabel id="colorBoxContainerLabel">Colors</ButtonBoxLabel>
        </ColorBox>
        <TextIndentContainer id="sidebarTextIndentContainer" column={'2/4'} row={7}>
          <ButtonBoxLabel id="marginContainerLabel">Margin</ButtonBoxLabel>
          <IndentButton column={2} row={2} directionIndex={0} isReset/>
          <IndentButton column={1} row={2} directionIndex={0} isReset={false} />
          <IndentButton column={2} row={1} directionIndex={1} isReset={false} />
          <IndentButton column={3} row={2} directionIndex={2} isReset={false} />
          <IndentButton column={2} row={3} directionIndex={3} isReset={false} />
        </TextIndentContainer>
      </GridContainer>
    </SideBarContainer>
  );
}

export default RightSidebar;
