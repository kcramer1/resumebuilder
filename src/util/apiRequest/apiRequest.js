import Amplify, { API, Auth } from 'aws-amplify';

Amplify.configure({
  API: {
    endpoints: [
      {
        name: 'journeyUserApi',
        endpoint: process.env.REACT_APP_env_api_profile_relationships
      },
      {
        name: 'journeySkillsApi',
        endpoint: process.env.REACT_APP_env_api_skills
      }
    ]
  }
});

const apiRequest = async function (
  apiName,
  endpoint,
  method,
  data = null
) {
  method = method.toLowerCase();
  const session = await Auth.currentSession();
  const idToken = session.getIdToken();
  const jwtToken = idToken.getJwtToken();
  const reqData = {
    headers: {
      'X-Cat-Ident': jwtToken
    },
    body: null
  };
  if (data) {
    reqData.body = data;
  }
  const result = await API[method](apiName, endpoint, reqData);
  return {
    data: result
  };
};

export default apiRequest;
