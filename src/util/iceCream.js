export const secondaryGrids = [
  {
    xRepeat: 6,
    yRepeat: 2,
    column: '7/10',
    row: '6/7',
    isActive: false,
    backgroundColor: '#eb9909',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  },
  {
    xRepeat: 10,
    yRepeat: 2,
    column: '6/11',
    row: '3/4',
    isActive: false,
    backgroundColor: '#09ebbc',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  },
  {
    xRepeat: 10,
    yRepeat: 2,
    column: '6/11',
    row: '4/5',
    isActive: false,
    backgroundColor: '#eb9909',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  },
  {
    xRepeat: 6,
    yRepeat: 2,
    column: '7/10',
    row: '7/8',
    isActive: false,
    backgroundColor: '#ffffff',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  },
  {
    xRepeat: 10,
    yRepeat: 2,
    column: '6/11',
    row: '5/6',
    isActive: false,
    backgroundColor: '#ffffff',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  },
  {
    xRepeat: 10,
    yRepeat: 2,
    column: '6/11',
    row: '2/3',
    isActive: false,
    backgroundColor: '#ffffff',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  },
  {
    xRepeat: 6,
    yRepeat: 2,
    column: '7/10',
    row: '1/2',
    isActive: false,
    backgroundColor: '#09ebbc',
    fontColor: '#ffffff',
    fontFamily: {
      value: 'Roboto',
      label: 'Roboto'
    },
    fontSize: {
      value: 'XLarge',
      label: 'XLarge',
      remSize: '4rem'
    },
    isBold: false,
    isItalic: false,
    isUnderlined: false,
    marginArray: [
      0,
      0,
      0,
      0
    ],
    primaryXRepeat: 16,
    primaryYRepeat: 16,
    text: ''
  }
]

export const tertiaryGrids = [
  [],
  [
    {
      column: '5/6',
      row: '1/2',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '2/3',
      row: '1/2',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '8/9',
      row: '2/3',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    }
  ],
  [],
  [
    {
      column: '2/6',
      row: '1/3',
      isActive: false,
      backgroundColor: '#eb9909',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    }
  ],
  [
    {
      column: '2/10',
      row: '1/3',
      isActive: false,
      backgroundColor: '#eb9909',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    }
  ],
  [
    {
      column: '2/10',
      row: '1/3',
      isActive: false,
      backgroundColor: '#09ebbc',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '3/4',
      row: '1/2',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '5/6',
      row: '2/3',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '8/9',
      row: '2/3',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    }
  ],
  [
    {
      column: '2/3',
      row: '1/2',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '4/5',
      row: '2/3',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    },
    {
      column: '6/7',
      row: '1/2',
      isActive: false,
      backgroundColor: '#4d4e42',
      fontColor: '#ffffff',
      fontFamily: {
        value: 'Roboto',
        label: 'Roboto'
      },
      fontSize: {
        value: 'XLarge',
        label: 'XLarge',
        remSize: '4rem'
      },
      isBold: false,
      isItalic: false,
      isUnderlined: false,
      marginArray: [
        0,
        0,
        0,
        0
      ],
      text: ''
    }
  ]
]